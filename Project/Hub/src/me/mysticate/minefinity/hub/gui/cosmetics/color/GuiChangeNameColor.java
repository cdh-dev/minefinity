package me.mysticate.minefinity.hub.gui.cosmetics.color;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.gui.buttons.BackButton;
import me.mysticate.minefinity.core.gui.buttons.BackButton.ButtonReturn;
import me.mysticate.minefinity.core.perk.PerkManager;
import me.mysticate.minefinity.core.rank.RankManager;
import me.mysticate.minefinity.hub.HubCosmeticManager;
import me.mysticate.minefinity.hub.gui.cosmetics.GuiLobbyCosmetics;

public class GuiChangeNameColor extends Gui
{
	private HubCosmeticManager _manager;
	private PerkManager _perkManager;
	private RankManager _rankManager;

	public GuiChangeNameColor(JavaPlugin plugin, Player player, HubCosmeticManager manager, PerkManager perkManager,
			RankManager rankManager)
	{
		super(plugin, player, "Select Color", 27);

		_manager = manager;
		_perkManager = perkManager;
		_rankManager = rankManager;
	}

	@Override
	protected void addItems()
	{
		borders(true);

		setItem(0, new BackButton(new ButtonReturn()
		{
			@Override
			public void onReturn(Gui gui, Player player, ClickType type)
			{
				new GuiLobbyCosmetics(getPlugin(), player, _manager.getHubManager(), _perkManager, _rankManager)
						.openInventory();
			}
		}, "Cosmetics"));

		for (int i = 0; i < _manager.getColors().length; i++)
		{
			ChatColor color = _manager.getColors()[i];

			if (i == 0)
			{
				setItem(8, new ButtonSelectNameColor(_manager, _perkManager, _rankManager, i, color));
			} else
			{
				addItem(new ButtonSelectNameColor(_manager, _perkManager, _rankManager, i, color));
			}
		}
	}

	@Override
	protected void onOpen()
	{

	}

	@Override
	protected void onClose()
	{

	}
}
