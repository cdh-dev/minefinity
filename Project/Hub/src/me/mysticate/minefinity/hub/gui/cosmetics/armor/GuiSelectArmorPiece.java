package me.mysticate.minefinity.hub.gui.cosmetics.armor;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.backpack.AquiredItem;
import me.mysticate.minefinity.core.common.misc.ArmorType;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.gui.buttons.BackButton;
import me.mysticate.minefinity.core.gui.buttons.BackButton.ButtonReturn;
import me.mysticate.minefinity.core.gui.buttons.NextButton;
import me.mysticate.minefinity.core.gui.buttons.NextButton.ButtonForeward;
import me.mysticate.minefinity.core.perk.PerkManager;
import me.mysticate.minefinity.core.rank.RankManager;
import me.mysticate.minefinity.hub.HubCosmeticManager;
import me.mysticate.minefinity.hub.HubManager;
import me.mysticate.minefinity.hub.gui.cosmetics.GuiLobbyCosmetics;

public class GuiSelectArmorPiece extends Gui
{
	private HubManager _hubManager;
	private PerkManager _perkManager;
	private RankManager _rankManager;
	private HubCosmeticManager _manager;
	
	private ArmorType _armor;
	private int _page;
	private List<AquiredItem> _items;
	
	public GuiSelectArmorPiece(JavaPlugin plugin, HubManager hubManager, PerkManager perkManager, RankManager rankManager, HubCosmeticManager manager, List<AquiredItem> items, ArmorType armor, int page, Player player)
	{
		super(plugin, player, "Page " + page, 45);
		
		_hubManager = hubManager;
		_perkManager = perkManager;
		_rankManager = rankManager;
		_manager = manager;
		
		_armor = armor;
		_page = page;
		_items = items;
	}

	@Override
	protected void addItems()
	{
		clear();
		borders(true);

		setItem(0, new BackButton(new ButtonReturn()
		{
			@Override
			public void onReturn(Gui gui, Player player, ClickType type)
			{
				(_page <= 1 ? new GuiLobbyCosmetics(getPlugin(), player, _hubManager, _perkManager, _rankManager) : new GuiSelectArmorPiece(getPlugin(), _hubManager, _perkManager, _rankManager, _manager, _items, _armor, _page - 1, player)).openInventory();
			}
		}, _page <= 1 ? "Cosmetics" : "Page " + (_page - 1)));

		setItem(4, new ButtonResetArmor(_manager, _armor));
		
		if (_items.size() >= (_page * 21))
		{
			setItem(8, new NextButton(new ButtonForeward()
			{
				@Override
				public void onForeward(Gui gui, Player player, ClickType type)
				{
					new GuiSelectArmorPiece(getPlugin(), _hubManager, _perkManager, _rankManager, _manager, _items, _armor, _page + 1, player).openInventory();
				}
			}, "Page " + (_page + 1)));
		}
		
		for (int i = 0 ; i < 21 ; i++)
		{
			final int ind = i + ((_page - 1) * 21);
			if (ind >= _items.size())
				break;
			
			addItem(new ButtonArmorPiece(_manager, _items.get(ind), _armor));
		}
		
		updateInventory();
	}

	@Override
	protected void onOpen()
	{
		
	}

	@Override
	protected void onClose()
	{
		
	}
}
