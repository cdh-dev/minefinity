package me.mysticate.minefinity.hub.gui.cosmetics;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.perk.PerkManager;
import me.mysticate.minefinity.core.rank.RankManager;
import me.mysticate.minefinity.hub.HubCosmeticManager;
import me.mysticate.minefinity.hub.gui.cosmetics.color.GuiChangeNameColor;

public class ButtonSelectNameColor implements Button
{
	private HubCosmeticManager _hubCosmeticManager;
	private PerkManager _perkManager;
	private RankManager _rankManager;

	public ButtonSelectNameColor(HubCosmeticManager hubCosmeticManager, PerkManager perkManager,
			RankManager rankManager)
	{
		_hubCosmeticManager = hubCosmeticManager;
		_perkManager = perkManager;
		_rankManager = rankManager;
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		return UtilItemStack.create(Material.INK_SACK, (byte) 5, 1, C.yellowBold + "Name Color",
				new String[] { " ", C.gray + "Add a splash of color to", C.gray + "the chat by changing your",
						C.gray + "name color in the chat!", C.gray + "Pretty..................", " ",
						C.green + "Click to select color" });
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		new GuiChangeNameColor(gui.getPlugin(), player, _hubCosmeticManager, _perkManager, _rankManager)
				.openInventory();
	}
}
