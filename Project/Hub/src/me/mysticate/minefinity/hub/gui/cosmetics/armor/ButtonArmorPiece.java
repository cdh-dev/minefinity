package me.mysticate.minefinity.hub.gui.cosmetics.armor;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.backpack.AquiredItem;
import me.mysticate.minefinity.core.common.misc.ArmorType;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.hub.HubCosmeticManager;

public class ButtonArmorPiece implements Button
{
	private HubCosmeticManager _manager;
	private AquiredItem _model;
	private ArmorType _armor;
	
	public ButtonArmorPiece(HubCosmeticManager manager, AquiredItem model, ArmorType armor)
	{
		_manager = manager;
		_model = model;
		_armor = armor;
	}
	
	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		return UtilItemStack.buildAquiredItem(_model);
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		_manager.setArmor(player, _armor, _model);
	}
}
