package me.mysticate.minefinity.hub.gui.cosmetics;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;

public class ButtonSelectHalo implements Button
{
	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		return UtilItemStack.create(Material.NETHER_STAR, (byte) 0, 1, C.yellowBold + "Halos",
				new String[] { C.red + "0" + C.white + "/" + C.aqua + "0" + C.green + " Owned", " ",
						C.gray + "Show off your angelic side!", C.gray + "Or your satanic one. Very fancy",
						C.gray + "either way.", " ", C.dRedBold + "COMING SOON" });
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{

	}
}
