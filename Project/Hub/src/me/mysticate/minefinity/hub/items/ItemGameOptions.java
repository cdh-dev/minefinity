package me.mysticate.minefinity.hub.items;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;

import me.mysticate.minefinity.MinefinityPlugin;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.item.ItemCallback;
import me.mysticate.minefinity.core.item.UseableItem;
import me.mysticate.minefinity.hub.gui.gameselect.GuiGameSelect;

public class ItemGameOptions extends UseableItem
{
	public ItemGameOptions()
	{
		super(Material.COMPASS, (byte) 0, new ItemCallback()
		{
			@Override
			public void onClick(Player player, Action type)
			{
				new GuiGameSelect(MinefinityPlugin.getPlugin(), player).openInventory();
			}
		});

		setDroppable(false);
		UtilItemStack.name(this, C.green + "Game Selector");
	}
}
