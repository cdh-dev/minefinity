package me.mysticate.minefinity.core.entity;

import org.bukkit.Location;

public interface ISpawnable
{
	public void spawn(Location loc);
}
