package me.mysticate.minefinity.core.perk;

public class Perk
{
	public String Name;
	public int Level;

	public Perk(String name)
	{
		this(name, 1);
	}

	public Perk(String name, int level)
	{
		Name = name;
		Level = level;
	}
}
