package me.mysticate.minefinity.core.commandsmisc.commands;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.commandsmisc.MiscCommandsManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.rank.Rank;

public class CommandGamemode extends CommandBase<MiscCommandsManager>
{
	public CommandGamemode(CommandManager commandManager, MiscCommandsManager manager)
	{
		super(commandManager, manager, new String[] { "gm", "gamemode", "creative", "survival" }, Rank.ADMIN, null);
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		Player target = player;

		if (args.length >= 1)
			target = Bukkit.getPlayer(args[0]);

		if (target == null)
		{
			sendMessage(player, "Failed to locate player " + C.yellow + args[0]);
			return true;
		}

		if (alias.equalsIgnoreCase("creative"))
		{
			target.setGameMode(GameMode.CREATIVE);
		} else if (alias.equalsIgnoreCase("survival"))
		{
			target.setGameMode(GameMode.SURVIVAL);
		} else
		{
			if (target.getGameMode() == GameMode.CREATIVE)
			{
				target.setGameMode(GameMode.SURVIVAL);
			} else
			{
				target.setGameMode(GameMode.CREATIVE);
			}
		}

		if (player == target)
		{
			sendMessage("Gamemode", player, "You set your gamemode to " + C.purple + F.gamemode(player.getGameMode()));
		} else
		{
			sendMessage("Gamemode", player, "You set " + C.yellow + target.getName() + "'s" + C.gray + " gamemode to "
					+ C.purple + F.gamemode(target.getGameMode()));
			sendMessage("Gamemode", target, "Your gamemode was set to " + C.purple + F.gamemode(target.getGameMode())
					+ " by " + C.yellow + player.getName() + ".");
		}

		return true;
	}
}
