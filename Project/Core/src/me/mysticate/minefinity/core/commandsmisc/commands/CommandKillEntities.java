package me.mysticate.minefinity.core.commandsmisc.commands;

import java.util.HashSet;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.commandsmisc.MiscCommandsManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilEntity;
import me.mysticate.minefinity.core.rank.Rank;

public class CommandKillEntities extends CommandBase<MiscCommandsManager>
{
	public CommandKillEntities(CommandManager commandManager, MiscCommandsManager manager)
	{
		super(commandManager, manager, new String[] { "killall", "butcher" }, Rank.ADMIN, null);
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		int totalRemoved = 0;
		for (Entity ent : new HashSet<Entity>(player.getWorld().getEntities()))
		{
			if (ent instanceof Player)
				continue;

			if (UtilEntity.canClear(ent))
				continue;

			ent.remove();
			totalRemoved++;
		}

		sendMessage("Kill", player, "You removed " + C.yellow + totalRemoved + C.gray + " entities.");
		return true;
	}
}
