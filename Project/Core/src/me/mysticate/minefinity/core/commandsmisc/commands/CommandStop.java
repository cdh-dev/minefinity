package me.mysticate.minefinity.core.commandsmisc.commands;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.commandsmisc.MiscCommandsManager;
import me.mysticate.minefinity.core.rank.Rank;

public class CommandStop extends CommandBase<MiscCommandsManager>
{
	public CommandStop(CommandManager commandManager, MiscCommandsManager manager)
	{
		super(commandManager, manager, new String[] { "stop", "reload" }, Rank.OWNER, null);

		setPass(true);
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		return false;
	}
}
