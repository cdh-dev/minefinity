package me.mysticate.minefinity.core.loadout;

import java.util.HashMap;
import java.util.UUID;

import me.mysticate.minefinity.core.file.DataFolder;
import me.mysticate.minefinity.core.file.YamlF;
import me.mysticate.minefinity.core.player.DataPlayer;

public class LoadoutPlayer extends DataPlayer
{
	private HashMap<String, Integer> _loadout = new HashMap<String, Integer>();

	public LoadoutPlayer(UUID id)
	{
		super(id, DataFolder.getLoadout(id));
	}

	public void setSlot(String slot, int id)
	{
		_loadout.put(slot, id);
	}

	public boolean hasSlot(String slot)
	{
		return _loadout.containsKey(slot);
	}

	public int getSlot(String slot)
	{
		return _loadout.get(slot);
	}

	@Override
	public void load()
	{
		for (String slot : readLines("loadout"))
		{
			int id = -1;
			try
			{
				id = Integer.valueOf(read(YamlF.path("loadout", slot), null));
			} catch (Exception ex)
			{
				ex.printStackTrace();
			}

			if (id == -1)
				continue;

			setSlot(slot, id);
		}
	}

	@Override
	public void save()
	{
		write("", "loadout");

		for (String slot : _loadout.keySet())
		{
			int id = _loadout.get(slot);
			write(id + "", YamlF.path("loadout", slot));
		}
	}
}
