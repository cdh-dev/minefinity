package me.mysticate.minefinity.core.chat.reader;

public enum ChatReaderResponse
{
	FAILED_TIMEOUT, FAILED_CANCEL, FAILED_LOGOUT, SUCCES
}
