package me.mysticate.minefinity.core.chat;

import java.util.UUID;

import me.mysticate.minefinity.core.player.PlayerData;

public class ChatPlayer extends PlayerData
{
	// TODO One day we should log player messages and show them in here

	private UUID _replyTo;

	public ChatPlayer(UUID id)
	{
		super(id);
	}

	public void setRepliesTo(UUID uuid)
	{
		_replyTo = uuid;
	}

	public UUID getRepliesTo()
	{
		return _replyTo;
	}
}
