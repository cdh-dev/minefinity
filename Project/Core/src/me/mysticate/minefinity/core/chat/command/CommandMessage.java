package me.mysticate.minefinity.core.chat.command;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.chat.ChatManager;
import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.rank.Rank;

public class CommandMessage extends CommandBase<ChatManager>
{
	public CommandMessage(CommandManager commandManager, ChatManager manager)
	{
		super(commandManager, manager, new String[] { "msg", "tell", "whisper", "message", "hotlinebling" },
				Rank.DEFAULT, "<Player> <Message>");
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		if (args.length < 2)
			return false;

		Player target = Bukkit.getPlayer(args[0]);
		if (target == null)
		{
			sendMessage(player, "That player could not be found.");
			return true;
		}

		String message = "";
		for (int i = 1; i < args.length; i++)
			message += (args[i] + " ");

		message.trim();

		getManager().getPlayer(player).setRepliesTo(target.getUniqueId());
		getManager().getPlayer(target).setRepliesTo(player.getUniqueId());

		target.sendMessage(C.dAquaBold + "PM " + C.underline + "from" + C.dAquaBold + " " + player.getName() + ": "
				+ C.aquaBold + message);
		player.sendMessage(C.dAquaBold + "PM " + C.underline + "to" + C.dAquaBold + " " + target.getName() + ": "
				+ C.aquaBold + message);
		return true;
	}
}
