package me.mysticate.minefinity.core.chat;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class ChatEvent extends PlayerEvent implements Cancellable
{
	private static HandlerList _handlers = new HandlerList();
	private boolean _cancelled = false;

	private static HandlerList getHandlerList()
	{
		return _handlers;
	}

	@Override
	public HandlerList getHandlers()
	{
		return getHandlerList();
	}

	@Override
	public void setCancelled(boolean cancelled)
	{
		_cancelled = cancelled;
	}

	@Override
	public boolean isCancelled()
	{
		return _cancelled;
	}

	public static enum MessageTarget
	{
		NONE, WHITELIST, BLACKLIST
	}

	private MessageTarget _target = MessageTarget.NONE;
	private Set<Player> _players = new HashSet<Player>();

	private boolean _displayRank = true;

	private String _prefix = "";

	private ChatColor _nameColor = ChatColor.YELLOW;

	private String _message;
	private ChatColor _messageColor = ChatColor.WHITE;

	public ChatEvent(Player player, String message)
	{
		super(player);

		_message = message;
	}

	public MessageTarget getTarget()
	{
		return _target;
	}

	public void setTarget(MessageTarget target)
	{
		_target = target;
	}

	public void addPlayer(Player player)
	{
		if (!_players.contains(player))
			_players.add(player);
	}

	public void removePlayer(Player player)
	{
		_players.remove(player);
	}

	public Set<Player> getPlayers()
	{
		return _players;
	}

	public boolean shouldDisplayRank()
	{
		return _displayRank;
	}

	public void setDisplayRank(boolean displayRank)
	{
		_displayRank = displayRank;
	}

	public String getPrefix()
	{
		return _prefix;
	}

	public void setPrefix(String prefix)
	{
		_prefix = prefix;
	}

	public ChatColor getNameColor()
	{
		return _nameColor;
	}

	public void setNameColor(ChatColor color)
	{
		_nameColor = color;
	}

	public String getMessage()
	{
		return _message;
	}

	public void setMessage(String message)
	{
		_message = message;
	}

	public ChatColor getMessageColor()
	{
		return _messageColor;
	}

	public void setMessageColor(ChatColor color)
	{
		_messageColor = color;
	}
}
