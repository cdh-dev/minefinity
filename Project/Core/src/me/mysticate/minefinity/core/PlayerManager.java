package me.mysticate.minefinity.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.player.IPlayer;
import me.mysticate.minefinity.core.punish.events.PlayerBannedLoginEvent;

public abstract class PlayerManager<Data extends IPlayer> extends Manager
{
	private Map<Player, Data> _data = new HashMap<Player, Data>();
	private final Object _dataLock = new Object();

	public PlayerManager(JavaPlugin plugin, String name)
	{
		super(plugin, name);
	}

	protected final Map<Player, Data> getData()
	{
		return _data;
	}

	protected final Object getLock()
	{
		return _dataLock;
	}

	@Override
	public void onEnable()
	{
		for (Player player : Bukkit.getOnlinePlayers())
		{
			synchronized (_dataLock)
			{
				_data.put(player, newData(player));
			}
		}
	}

	@Override
	public void onDisable()
	{
		for (Player player : new HashSet<Player>(_data.keySet()))
		{
			synchronized (_dataLock)
			{
				_data.remove(player);
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onLogin(PlayerLoginEvent event)
	{
		synchronized (_dataLock)
		{
			_data.put(event.getPlayer(), newData(event.getPlayer()));
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onQuit(PlayerQuitEvent event)
	{
		synchronized (_dataLock)
		{
			_data.remove(event.getPlayer());
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onBanned(PlayerBannedLoginEvent event)
	{
		synchronized (_dataLock)
		{
			_data.remove(event.getPlayer());
		}
	}

	public Data getPlayer(Player player)
	{
		synchronized (_dataLock)
		{
			return _data.get(player);
		}
	}

	public Data getPlayer(String player)
	{
		synchronized (_dataLock)
		{
			for (Player cur : _data.keySet())
			{
				if (cur.getName().equalsIgnoreCase(player))
				{
					return _data.get(cur);
				}
			}
		}

		return null;
	}

	protected abstract Data newData(Player player);
}