package me.mysticate.minefinity.core;

import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.F;

public abstract class Manager implements Listener
{
	private static HashSet<Manager> _loadedManagers = new HashSet<Manager>();

	private static void addManager(Manager manager)
	{
		if (!_loadedManagers.contains(manager))
			_loadedManagers.add(manager);
	}

	private static void removeManager(Manager manager)
	{
		_loadedManagers.remove(manager);
	}

	public static void killManagers()
	{
		for (Manager manager : new HashSet<Manager>(_loadedManagers))
		{
			manager.setEnabled(false);
		}
	}

	private JavaPlugin _plugin;
	private String _name;

	private boolean _enabled = false;

	public Manager(JavaPlugin plugin, String name)
	{
		_plugin = plugin;
		_name = name;

		Bukkit.getScheduler().runTaskLater(plugin, new Runnable()
		{
			@Override
			public void run()
			{
				setEnabled(true);
			}
		}, 1);
	}

	public JavaPlugin getPlugin()
	{
		return _plugin;
	}

	public String getName()
	{
		return _name;
	}

	public boolean isEnabled()
	{
		return _enabled;
	}

	public void setEnabled(boolean enabled)
	{
		if (_enabled == enabled)
			return;

		_enabled = enabled;

		if (_enabled)
		{
			IM.add(this);
			Manager.addManager(this);

			registerCommands(IM.command());

			Bukkit.getPluginManager().registerEvents(this, _plugin);
			onEnable();
		} else
		{
			IM.remove(this);
			Manager.removeManager(this);

			HandlerList.unregisterAll(this);
			onDisable();
		}
	}

	protected void onDisable()
	{
	}

	protected void onEnable()
	{
	}

	protected void registerCommands(CommandManager commandManager)
	{
	}

	public void runAsync(Runnable run)
	{
		Bukkit.getScheduler().runTaskAsynchronously(_plugin, run);
	}

	public void runTask(Runnable run)
	{
		Bukkit.getScheduler().runTask(_plugin, run);
	}

	public void sendMessage(Player player, String message)
	{
		player.sendMessage(F.manager(getName(), message));
	}
}
