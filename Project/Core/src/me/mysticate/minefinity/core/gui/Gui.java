package me.mysticate.minefinity.core.gui;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.buttons.ItemButton;

public abstract class Gui implements Listener
{
	private JavaPlugin _plugin;

	private Inventory _inv;
	private Button[] _buttons;

	private Player _player;

	public Gui(JavaPlugin plugin, Player player)
	{
		this(plugin, player, "Inventory");
	}

	public Gui(JavaPlugin plugin, Player player, String name)
	{
		this(plugin, player, name, 27);
	}

	public Gui(JavaPlugin plugin, Player player, String name, int size)
	{
		_plugin = plugin;

		_inv = Bukkit.createInventory(null, size, name);
		_buttons = new Button[size];

		_player = player;
	}

	public JavaPlugin getPlugin()
	{
		return _plugin;
	}

	public Inventory getInventory()
	{
		return _inv;
	}

	public Button[] getButtons()
	{
		return _buttons;
	}

	public Player getPlayer()
	{
		return _player;
	}

	public void openInventory()
	{
		clear();

		addItems();
		updateInventory();

		_player.openInventory(_inv);
		_player.updateInventory();

		onOpen();

		HandlerList.unregisterAll(this);
		Bukkit.getPluginManager().registerEvents(this, _plugin);
	}

	@EventHandler
	public void onClose(InventoryCloseEvent event)
	{
		if (_player == event.getPlayer() && event.getInventory().equals(_inv))
		{
			HandlerList.unregisterAll(this);
			onClose();
		}
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event)
	{
		if (_player == event.getPlayer())
		{
			HandlerList.unregisterAll(this);
			onClose();
		}
	}

	@EventHandler
	public void onClick(InventoryClickEvent event)
	{
		if (_player == event.getWhoClicked() && event.getClickedInventory() != null
				&& event.getClickedInventory().equals(_inv))
		{
			event.setCancelled(true);

			Button button = _buttons[event.getRawSlot()];
			if (button == null)
				return;

			button.onClick(this, _player, event.getClick());
		}
	}

	public void updateInventory()
	{
		for (int i = 0; i < _buttons.length; i++)
		{
			Button button = _buttons[i];

			if (button == null)
			{
				_inv.setItem(i, null);
				continue;
			}

			_inv.setItem(i, button.getItem(this, _player));
		}
	}

	public void setItem(int slot, Button button)
	{
		if (slot >= _inv.getSize() || slot < 0)
			return;

		_buttons[slot] = button;
	}

	public void addItem(Button button)
	{
		for (int i = 0; i < _buttons.length; i++)
		{
			Button current = _buttons[i];
			if (current != null)
				continue;

			setItem(i, button);
			return;
		}
	}

	public void borders(boolean bottom)
	{
		for (int i = 0; i < _buttons.length; i++)
		{
			Button current = _buttons[i];
			if (current != null)
				continue;

			if (i < 9 || i % 9 == 0 || i % 9 == 8 || (bottom && i >= (_buttons.length - 9)))
				setItem(i, new ItemButton(UtilItemStack.create(Material.STAINED_GLASS_PANE, (byte) 15, 1, C.strike)));
		}
	}

	public void clear()
	{
		_buttons = new Button[_inv.getSize()];
	}

	protected abstract void addItems();

	protected abstract void onOpen();

	protected abstract void onClose();
}
