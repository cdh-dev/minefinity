package me.mysticate.minefinity.core.gui.buttons;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;

public class BackButton implements Button
{
	public static interface ButtonReturn
	{
		public void onReturn(Gui gui, Player player, ClickType type);
	}

	private ButtonReturn _back;
	private String _name;

	public BackButton(ButtonReturn back, String name)
	{
		_back = back;
		_name = name;
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		return UtilItemStack.create(Material.WOOD_BUTTON, (byte) 0, 1,
				C.green + "\u25C0 " + C.white + "Return to " + C.purple + _name);
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		_back.onReturn(gui, player, type);
	}
}
