package me.mysticate.minefinity.core.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

public interface Button
{
	public ItemStack getItem(Gui gui, Player player);

	public void onClick(Gui gui, Player player, ClickType type);
}