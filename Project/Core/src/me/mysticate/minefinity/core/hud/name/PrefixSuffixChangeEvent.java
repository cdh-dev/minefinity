package me.mysticate.minefinity.core.hud.name;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PrefixSuffixChangeEvent extends Event
{
	private static HandlerList _handlers = new HandlerList();

	private static HandlerList getHandlerList()
	{
		return _handlers;
	}

	@Override
	public HandlerList getHandlers()
	{
		return getHandlerList();
	}

	private PrefixSuffixModule _module;

	public PrefixSuffixChangeEvent(PrefixSuffixModule module)
	{
		_module = module;
	}

	public PrefixSuffixModule getModule()
	{
		return _module;
	}
}
