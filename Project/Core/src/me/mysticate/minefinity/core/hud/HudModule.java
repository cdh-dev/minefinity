package me.mysticate.minefinity.core.hud;

import org.bukkit.entity.Player;

public abstract class HudModule
{
	private Player _player;

	public HudModule(Player player)
	{
		_player = player;
	}

	public Player getPlayer()
	{
		return _player;
	}
}
