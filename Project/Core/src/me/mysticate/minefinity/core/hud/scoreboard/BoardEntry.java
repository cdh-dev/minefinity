package me.mysticate.minefinity.core.hud.scoreboard;

import org.bukkit.ChatColor;

class BoardEntry
{
	private String _prefix = "";
	private String _suffix = "";

	public BoardEntry(String line)
	{
		_prefix = line.substring(0, Math.min(line.length(), 16));

		String rawSuffix = line.length() > 16 ? line.substring(16, line.length()) : "";
		rawSuffix = ChatColor.getLastColors(_prefix) + rawSuffix;
		_suffix = rawSuffix.substring(0, Math.min(rawSuffix.length(), 16));
	}

	public String getPrefix()
	{
		return _prefix;
	}

	public String getSuffix()
	{
		return _suffix;
	}
}
