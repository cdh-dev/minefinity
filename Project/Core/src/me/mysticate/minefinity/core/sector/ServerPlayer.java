package me.mysticate.minefinity.core.sector;

import java.util.UUID;

import me.mysticate.minefinity.core.player.PlayerData;

public class ServerPlayer extends PlayerData
{
	private Sector _sector;
	private Sector _lastSector;

	public ServerPlayer(UUID id)
	{
		super(id);
	}

	public Sector getSector()
	{
		return _sector;
	}

	public Sector getLastSector()
	{
		return _lastSector;
	}

	public void setSector(Sector sector)
	{
		if (_sector != sector)
			_lastSector = _sector;

		_sector = sector;
	}
}
