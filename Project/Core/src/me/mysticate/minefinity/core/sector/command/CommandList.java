package me.mysticate.minefinity.core.sector.command;

import java.util.List;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.rank.Rank;
import me.mysticate.minefinity.core.rank.RankManager;
import me.mysticate.minefinity.core.sector.Sector;
import me.mysticate.minefinity.core.sector.SectorManager;
import me.mysticate.minefinity.core.sector.SectorWorld;

public class CommandList extends CommandBase<SectorManager>
{
	private RankManager _rankManager;

	public CommandList(CommandManager commandManager, SectorManager manager, RankManager rankManager)
	{
		super(commandManager, manager, new String[] { "list", "online" }, Rank.MOD, "<ID>");

		_rankManager = rankManager;
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		if (args.length == 0)
			return false;

		String name = args[0];
		String[] parts = name.split("-");

		try
		{
			Sector sector = Sector.getByName(parts[0]);
			int id = Integer.valueOf(parts[1]);

			SectorWorld sworld = getManager().getWorld(sector, id);

			String list = "";

			List<Player> players = sworld.getWorld().getPlayers();
			for (int i = 0; i < players.size(); i++)
			{
				Player target = players.get(i);
				list += (_rankManager.getPlayer(target).getDominant().getColor() + target.getName()
						+ (i == players.size() - 1 ? "" : (C.gray + ",")));
			}

			player.sendMessage(" ");
			player.sendMessage(C.blueBold + "Online at " + sector.getName() + "-" + id + ":");
			player.sendMessage(list);
		} catch (Exception ex)
		{
			sendMessage(player, "Invalid ID");
		}
		return true;
	}
}
