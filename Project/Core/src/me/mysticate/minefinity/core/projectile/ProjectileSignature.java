package me.mysticate.minefinity.core.projectile;

import org.bukkit.Bukkit;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import me.mysticate.minefinity.MinefinityPlugin;
import me.mysticate.minefinity.core.combat.event.CustomDamageEvent;

public class ProjectileSignature implements Listener
{
	private static ProjectileSignature _instance;

	public static ProjectileSignature instance()
	{
		if (_instance == null)
			_instance = new ProjectileSignature();

		return _instance;
	}

	private ProjectileSignature()
	{
		Bukkit.getPluginManager().registerEvents(this, MinefinityPlugin.getPlugin());
	}

	public void sign(Projectile proj, IProjectileCallback callback)
	{
		proj.setMetadata("dankmemessignatureprojectiles",
				new FixedMetadataValue(MinefinityPlugin.getPlugin(), callback));
	}

	@EventHandler
	public void onDamage(CustomDamageEvent event)
	{
		if (event.hasProjectile())
		{
			if (event.getProjectile().hasMetadata("dankmemessignatureprojectiles"))
			{
				for (MetadataValue value : event.getProjectile().getMetadata("dankmemessignatureprojectiles"))
				{
					if (value.value() instanceof IProjectileCallback)
					{
						((IProjectileCallback) value.value()).onLand(event.getProjectile(), event.getDamagee());
					}
				}
			}
		}
	}

	@EventHandler
	public void onLand(ProjectileHitEvent event)
	{
		if (event.getEntity().hasMetadata("dankmemessignatureprojectiles"))
		{
			for (MetadataValue value : event.getEntity().getMetadata("dankmemessignatureprojectiles"))
			{
				if (value.value() instanceof IProjectileCallback)
				{
					((IProjectileCallback) value.value()).onLand(event.getEntity(), null);
				}
			}
		}
	}
}
