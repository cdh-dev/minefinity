package me.mysticate.minefinity.core.projectile;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Projectile;

public interface IProjectileCallback
{
	public void onLand(Projectile proj, Entity hit);
}
