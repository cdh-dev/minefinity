package me.mysticate.minefinity.core.updater.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.mysticate.minefinity.core.updater.UpdateType;

public class TimeEvent extends Event
{
	private static HandlerList _handlerList = new HandlerList();

	private static HandlerList getHandlerList()
	{
		return _handlerList;
	}

	@Override
	public HandlerList getHandlers()
	{
		return getHandlerList();
	}

	private UpdateType _type;

	public TimeEvent(UpdateType type)
	{
		_type = type;
	}

	public UpdateType getType()
	{
		return _type;
	}
}
