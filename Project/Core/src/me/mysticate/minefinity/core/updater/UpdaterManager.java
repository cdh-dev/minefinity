package me.mysticate.minefinity.core.updater;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.Manager;
import me.mysticate.minefinity.core.updater.event.TimeEvent;

public class UpdaterManager extends Manager implements Runnable
{
	private int _lock;
	private boolean _timing = false;

	public UpdaterManager(JavaPlugin plugin)
	{
		super(plugin, "Updater");
	}

	public boolean isDoingTiming()
	{
		return _timing;
	}

	@Override
	public void onEnable()
	{
		_lock = Bukkit.getScheduler().scheduleSyncRepeatingTask(getPlugin(), this, 0l, 1l);
	}

	@Override
	public void onDisable()
	{
		try
		{
			Bukkit.getScheduler().cancelTask(_lock);
		} catch (NullPointerException ex)
		{

		}
	}

	@Override
	public void run()
	{
		_timing = true;

		for (UpdateType type : UpdateType.values())
		{
			if (type.elapsed(this))
			{
				Bukkit.getPluginManager().callEvent(new TimeEvent(type));
			}
		}

		_timing = false;
	}
}
