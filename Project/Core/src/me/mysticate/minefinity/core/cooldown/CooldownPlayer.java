package me.mysticate.minefinity.core.cooldown;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.player.PlayerData;

public class CooldownPlayer extends PlayerData
{
	private Player _player;
	private HashMap<CooldownPool, Map<String, Cooldown>> _cooldowns = new HashMap<CooldownPool, Map<String, Cooldown>>();

	public CooldownPlayer(Player player)
	{
		super(player.getUniqueId());

		for (CooldownPool pool : CooldownPool.values())
			_cooldowns.put(pool, new HashMap<String, Cooldown>());

		_player = player;
	}

	public void tick()
	{
		for (CooldownPool pool : _cooldowns.keySet())
		{
			for (String name : new HashSet<String>(_cooldowns.get(pool).keySet()))
			{
				check(pool, name);
			}
		}
	}

	private void check(CooldownPool pool, String name)
	{
		Cooldown cooldown = _cooldowns.get(pool).get(name);
		if (cooldown == null)
			return;

		if (cooldown.elapsed())
		{
			_cooldowns.get(pool).remove(name);

			if (cooldown.hasName())
			{
				_player.sendMessage(C.blueBold + "Cooldown: " + C.white + "You can now use " + C.green
						+ cooldown.getName() + C.white + ".");
			}
			Bukkit.getPluginManager().callEvent(new CooldownEvent(_player, name, pool, cooldown));
		}
	}

	public boolean addCooldown(String name, Cooldown cooldown)
	{
		return addCooldown(CooldownPool.ALL, name, cooldown);
	}

	public boolean addCooldown(CooldownPool pool, String name, Cooldown cooldown)
	{
		check(pool, name);

		if (_cooldowns.get(pool).containsKey(name))
			return false;

		_cooldowns.get(pool).put(name, cooldown);
		return true;
	}

	public void forceAddCooldown(String name, Cooldown cooldown)
	{
		forceAddCooldown(CooldownPool.ALL, name, cooldown);
	}

	public void forceAddCooldown(CooldownPool pool, String name, Cooldown cooldown)
	{
		check(pool, name);
		_cooldowns.get(pool).put(name, cooldown);
	}

	public void removeCooldown(String name)
	{
		removeCooldown(CooldownPool.ALL, name);
	}

	public void removeCooldown(CooldownPool pool, String name)
	{
		check(pool, name);
		_cooldowns.get(pool).remove(name);
	}

	public boolean hasCooldown(String name)
	{
		return hasCooldown(CooldownPool.ALL, name);
	}

	public boolean hasCooldown(CooldownPool pool, String name)
	{
		check(pool, name);
		return _cooldowns.get(pool).containsKey(name);
	}

	public Cooldown getCooldown(String name)
	{
		return getCooldown(CooldownPool.ALL, name);
	}

	public Cooldown getCooldown(CooldownPool pool, String name)
	{
		return _cooldowns.get(pool).get(name);
	}

	public void clear(CooldownPool pool)
	{
		_cooldowns.get(pool).clear();
	}
}
