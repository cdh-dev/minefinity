package me.mysticate.minefinity.core.cooldown;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class CooldownEvent extends PlayerEvent
{
	private static HandlerList _handlers = new HandlerList();

	private static HandlerList getHandlerList()
	{
		return _handlers;
	}

	@Override
	public HandlerList getHandlers()
	{
		return getHandlerList();
	}

	private String _name;
	private CooldownPool _pool;
	private Cooldown _cooldown;

	public CooldownEvent(Player who, String name, CooldownPool pool, Cooldown cooldown)
	{
		super(who);

		_name = name;
		_pool = pool;
		_cooldown = cooldown;
	}

	public String getName()
	{
		return _name;
	}

	public CooldownPool getPool()
	{
		return _pool;
	}

	public Cooldown getCooldown()
	{
		return _cooldown;
	}
}
