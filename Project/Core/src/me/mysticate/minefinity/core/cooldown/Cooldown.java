package me.mysticate.minefinity.core.cooldown;

import me.mysticate.minefinity.core.common.util.UtilTime;

public class Cooldown
{
	private long _timestamp = System.currentTimeMillis();
	private long _period;

	private String _name = null;

	public Cooldown(long period)
	{
		_period = period;
	}

	public long getTimestamp()
	{
		return _timestamp;
	}

	public long getPeriod()
	{
		return _period;
	}

	public boolean hasName()
	{
		return _name != null;
	}

	public String getName()
	{
		return _name;
	}

	public void setName(String name)
	{
		_name = name;
	}

	public boolean elapsed()
	{
		return UtilTime.hasPassed(_timestamp, _period);
	}

	public float asFloat()
	{
		return (float) (System.currentTimeMillis() - _timestamp) / _period;
	}
}
