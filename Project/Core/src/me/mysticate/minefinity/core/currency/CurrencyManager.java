package me.mysticate.minefinity.core.currency;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.DataPlayerManager;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.currency.command.CurrencyCommand;

public class CurrencyManager extends DataPlayerManager<CurrencyPlayer>
{
	public CurrencyManager(JavaPlugin plugin)
	{
		super(plugin, "Currency");
	}

	@Override
	public void registerCommands(CommandManager commandManager)
	{
		commandManager.registerCommand(new CurrencyCommand(commandManager, this));
	}

	@Override
	protected CurrencyPlayer newData(Player player)
	{
		return new CurrencyPlayer(player.getUniqueId());
	}

	@Override
	protected CurrencyPlayer newOfflineData(UUID uuid)
	{
		return new CurrencyPlayer(uuid);
	}
}