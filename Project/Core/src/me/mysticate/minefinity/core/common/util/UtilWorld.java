package me.mysticate.minefinity.core.common.util;

import org.bukkit.World;
import org.bukkit.entity.Player;

public class UtilWorld
{
	public static void sendMessage(World world, String message)
	{
		for (Player player : world.getPlayers())
		{
			player.sendMessage(message);
		}
	}
}
