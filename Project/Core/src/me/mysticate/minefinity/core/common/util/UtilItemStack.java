package me.mysticate.minefinity.core.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.enchantments.EnchantmentWrapper;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import me.mysticate.minefinity.core.backpack.AquiredItem;
import me.mysticate.minefinity.core.backpack.DefaultMeta;
import me.mysticate.minefinity.core.common.text.C;

public class UtilItemStack
{
	private static class GlowEnchant extends EnchantmentWrapper
	{
		public GlowEnchant(int id)
		{
			super(id);
		}

		public boolean canEnchantItem(ItemStack item)
		{
			return true;
		}

		public boolean conflictsWith(Enchantment other)
		{
			return false;
		}

		public EnchantmentTarget getItemTarget()
		{
			return null;
		}

		public int getMaxLevel()
		{
			return 10;
		}

		public String getName()
		{
			return "Glow";
		}

		public int getStartLevel()
		{
			return 1;
		}
	}

	private static GlowEnchant _glow;

	static
	{
		_glow = new GlowEnchant(420);

		UtilReflection.setField(Enchantment.class, "acceptingNew", true);
		Enchantment.registerEnchantment(_glow);
	}

	public static Enchantment getGlow()
	{
		return _glow;
	}

	public static ItemStack name(ItemStack stack, String name)
	{
		if (name == null)
			return stack;

		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(name);
		stack.setItemMeta(meta);
		return stack;
	}

	public static List<String> getLore(ItemStack stack)
	{
		List<String> lore = new ArrayList<String>();

		if (!stack.hasItemMeta())
			return lore;

		if (!stack.getItemMeta().hasLore())
			return lore;

		lore.addAll(stack.getItemMeta().getLore());
		return lore;
	}

	public static ItemStack setLore(ItemStack stack, List<String> lore)
	{
		ItemMeta meta = stack.getItemMeta();
		meta.setLore(lore);
		stack.setItemMeta(meta);
		return stack;
	}
	
	public static ItemStack setName(ItemStack stack, String name)
	{
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(name);
		stack.setItemMeta(meta);
		return stack;
	}

	public static ItemStack unbreakable(ItemStack stack)
	{
		ItemMeta meta = stack.getItemMeta();
		meta.spigot().setUnbreakable(true);
		stack.setItemMeta(meta);
		return stack;
	}

	public static ItemStack setLore(ItemStack stack, String[] lore)
	{
		return setLore(stack, Arrays.asList(lore));
	}

	public static ItemStack create(Material mat)
	{
		return hideAttributes(create(mat, 1));
	}

	public static ItemStack create(Material mat, int amount)
	{
		return hideAttributes(create(mat, (byte) 0, amount));
	}

	public static ItemStack create(Material mat, byte data, int amount)
	{
		return hideAttributes(new ItemStack(mat, amount, data));
	}

	public static ItemStack create(Material mat, byte data, int amount, String name)
	{
		return hideAttributes(name(create(mat, data, amount), name));
	}

	public static ItemStack create(Material mat, byte data, int amount, String name, String[] lore)
	{
		return hideAttributes(setLore(create(mat, data, amount, name), lore));
	}

	public static ItemStack hideAttributes(ItemStack item)
	{
		ItemMeta meta = item.getItemMeta();

		for (ItemFlag flag : ItemFlag.values())
			meta.addItemFlags(flag);

		item.setItemMeta(meta);
		return item;
	}

	public static ItemStack glow(ItemStack item)
	{
		item.addEnchantment(getGlow(), 1);
		return item;
	}

	public static ItemStack color(ItemStack item, Color color)
	{
		if (item.getItemMeta() instanceof LeatherArmorMeta)
		{
			LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
			meta.setColor(color);
			item.setItemMeta(meta);
		}
		return item;
	}

	public static boolean isBow(ItemStack item)
	{
		return item.getType() == Material.BOW;
	}

	public static boolean isSword(ItemStack item)
	{
		return item.getType().toString().contains("_SWORD");
	}

	public static boolean isAxe(ItemStack item)
	{
		return item.getType().toString().contains("_AXE");
	}

	public static String getName(Material mat)
	{
		String[] parts = mat.toString().split("_");
		String string = "";

		for (int i = 0; i < parts.length; i++)
		{
			String part = parts[i];
			string += (part.substring(0, 1).toUpperCase() + part.substring(1).toLowerCase()
					+ (i == parts.length - 1 ? "" : " "));
		}
		return string;
	}
	
	public static ItemStack buildAquiredItem(AquiredItem item)
	{
		try
		{
			Material mat = Material.valueOf(item.getValue(DefaultMeta.MATERIAL).asString().toUpperCase());
			byte data = Byte.valueOf(item.getValue(DefaultMeta.DATA, (byte) 0).asString());
			int amount = item.getValue(DefaultMeta.AMOUNT, 1).asInt();
			
			String name = C.yellow + ChatColor.translateAlternateColorCodes('&', item.getDisplayName());
			String[] lore = item.getValue(DefaultMeta.LORE, "").asString().split("||");
			for (int i = 0 ; i < lore.length ; i++)
				lore[i] = C.white + lore[i];		
			
			boolean glow = item.hasGlow();
			
			ItemStack stack = UtilItemStack.create(mat, data, amount, name, lore);
			if (glow)
				stack.addEnchantment(UtilItemStack.getGlow(), 1);
			
			return stack;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			return UtilItemStack.create(Material.BARRIER, (byte) 0, 1, " ");
		}
	}
}
