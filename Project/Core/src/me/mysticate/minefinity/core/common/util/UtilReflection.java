package me.mysticate.minefinity.core.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class UtilReflection
{
	public static void setField(Object target, String fieldName, Object value)
	{
		try
		{
			Field field = target.getClass().getDeclaredField(fieldName);
			final boolean accessible = field.isAccessible();

			field.setAccessible(true);
			field.set(target, value);

			field.setAccessible(accessible);
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void setField(Class<?> target, String fieldName, Object value)
	{
		try
		{
			Field field = target.getDeclaredField(fieldName);
			final boolean accessible = field.isAccessible();

			field.setAccessible(true);
			field.set(null, value);

			field.setAccessible(accessible);
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void callMethod(Object target, String name, Object... args)
	{
		try
		{
			Class<?>[] clazzs = new Class<?>[args.length];
			for (int i = 0; i < args.length; i++)
				clazzs[i] = args[i].getClass();

			Method method = target.getClass().getDeclaredMethod(name, clazzs);
			final boolean accessible = method.isAccessible();

			method.setAccessible(true);

			method.invoke(target, args);
			method.setAccessible(accessible);
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void callMethod(Class<?> target, String name, Object... args)
	{
		try
		{
			Class<?>[] clazzs = new Class<?>[args.length];
			for (int i = 0; i < args.length; i++)
			{
				clazzs[i] = args[i].getClass();
			}

			Method method = target.getDeclaredMethod(name, clazzs);
			final boolean accessible = method.isAccessible();

			method.setAccessible(true);

			method.invoke(null, args);
			method.setAccessible(accessible);
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void callMethod(Class<?> target, String name, Class<?>[] clazz, Object[] objs)
	{
		try
		{
			Method method = target.getDeclaredMethod(name, clazz);
			final boolean accessible = method.isAccessible();

			method.setAccessible(true);

			method.invoke(null, objs);
			method.setAccessible(accessible);
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static void callMethod(Object target, String name, Class<?>[] clazz, Object[] objs)
	{
		try
		{
			Method method = target.getClass().getDeclaredMethod(name, clazz);
			final boolean accessible = method.isAccessible();

			method.setAccessible(true);

			method.invoke(target, objs);
			method.setAccessible(accessible);
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static Object getField(Object target, String name)
	{
		try
		{
			Field field = target.getClass().getDeclaredField(name);
			field.setAccessible(true);
			return field.get(target);
		} catch (Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
	}

	public static Object getField(Class<?> target, String name)
	{
		try
		{
			Field field = target.getClass().getDeclaredField(name);
			field.setAccessible(true);
			return field.get(null);
		} catch (Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
}
