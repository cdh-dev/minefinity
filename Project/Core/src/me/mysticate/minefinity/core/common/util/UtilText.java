package me.mysticate.minefinity.core.common.util;

public class UtilText
{
	public static boolean onlyValidCharacters(String string)
	{
		char[] allowed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 ".toCharArray();

		char[] chars = string.toCharArray();
		for (char cur : chars)
		{
			boolean pass = false;
			for (char gud : allowed)
			{
				if (cur == gud)
					pass = true;
			}

			if (!pass)
				return false;
		}

		return true;
	}

	public static boolean validate(String string, int minLength, int maxLength, boolean validCharacters)
	{
		return ((string.length() >= minLength) && (string.length() <= maxLength)
				&& (onlyValidCharacters(string) == validCharacters));
	}
}
