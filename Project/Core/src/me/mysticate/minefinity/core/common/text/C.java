package me.mysticate.minefinity.core.common.text;

import org.bukkit.ChatColor;

public class C
{
	public static String green = "" + ChatColor.GREEN;
	public static String aqua = "" + ChatColor.AQUA;
	public static String red = "" + ChatColor.RED;
	public static String purple = "" + ChatColor.LIGHT_PURPLE;
	public static String yellow = "" + ChatColor.YELLOW;
	public static String white = "" + ChatColor.WHITE;

	public static String black = "" + ChatColor.BLACK;
	public static String dBlue = "" + ChatColor.DARK_BLUE;
	public static String dGreen = "" + ChatColor.DARK_GREEN;
	public static String dAqua = "" + ChatColor.DARK_AQUA;
	public static String dRed = "" + ChatColor.DARK_RED;
	public static String dPurple = "" + ChatColor.DARK_PURPLE;
	public static String gold = "" + ChatColor.GOLD;
	public static String gray = "" + ChatColor.GRAY;
	public static String dGray = "" + ChatColor.DARK_GRAY;
	public static String blue = "" + ChatColor.BLUE;

	public static String bold = "" + ChatColor.BOLD;
	public static String underline = "" + ChatColor.UNDERLINE;
	public static String italic = "" + ChatColor.ITALIC;
	public static String magic = "" + ChatColor.MAGIC;
	public static String strike = "" + ChatColor.STRIKETHROUGH;
	public static String reset = "" + ChatColor.RESET;

	public static String greenBold = green + bold;
	public static String aquaBold = aqua + bold;
	public static String redBold = red + bold;
	public static String purpleBold = purple + bold;
	public static String yellowBold = yellow + bold;
	public static String whiteBold = white + bold;
	public static String blackBold = black + bold;
	public static String dBlueBold = dBlue + bold;
	public static String dGreenBold = dGreen + bold;
	public static String dAquaBold = dAqua + bold;
	public static String dRedBold = dRed + bold;
	public static String dPurpleBold = dPurple + bold;
	public static String goldBold = gold + bold;
	public static String grayBold = gray + bold;
	public static String dGrayBold = dGray + bold;
	public static String blueBold = blue + bold;
}
