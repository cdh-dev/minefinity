package me.mysticate.minefinity.core.common.util;

import org.bukkit.ChatColor;

public class UtilColor
{
	public static byte getBannerData(ChatColor color)
	{
		switch (color)
		{
		case BLACK:
			return 0;
		case DARK_RED:
			return 1;
		case DARK_GREEN:
			return 2;
		case DARK_BLUE:
			return 4;
		case DARK_PURPLE:
			return 5;
		case DARK_AQUA:
			return 6;
		case GRAY:
			return 7;
		case DARK_GRAY:
			return 8;
		case RED:
			return 9;
		case GREEN:
			return 10;
		case YELLOW:
			return 11;
		case AQUA:
			return 12;
		case LIGHT_PURPLE:
			return 13;
		case GOLD:
			return 14;
		case WHITE:
			return 15;
		default:
			return 15;
		}
	}
}
