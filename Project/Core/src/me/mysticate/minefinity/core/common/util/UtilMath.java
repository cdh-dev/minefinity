package me.mysticate.minefinity.core.common.util;

import java.util.List;
import java.util.Random;

public class UtilMath
{
	private static Random _random = new Random();

	public static Random getRandom()
	{
		return _random;
	}

	public static <O> O getRandom(List<O> list)
	{
		if (list == null || list.isEmpty())
			return null;

		return list.get(getRandom().nextInt(list.size()));
	}

	public static <O> O getRandom(O[] list)
	{
		if (list == null || list.length == 0)
			return null;

		return list[getRandom().nextInt(list.length)];
	}
}
