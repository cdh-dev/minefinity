package me.mysticate.minefinity.core.combat.event;

import java.util.HashMap;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDamageEvent.DamageModifier;
import org.bukkit.inventory.ItemStack;

public class CustomDamageEvent extends Event implements Cancellable
{
	private static HandlerList _handlers = new HandlerList();
	private boolean _cancelled = false;

	private static HandlerList getHandlerList()
	{
		return _handlers;
	}

	@Override
	public HandlerList getHandlers()
	{
		return getHandlerList();
	}

	@Override
	public boolean isCancelled()
	{
		return _cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled)
	{
		_cancelled = cancelled;
	}

	private Entity _damagee;
	private DamageCause _cause;

	private Entity _directDamager;
	private Entity _damager;
	private Projectile _proj;

	private ItemStack _item;

	private HashMap<DamageModifier, Double> _defaultModifiers;

	public CustomDamageEvent(Entity damagee, DamageCause cause, Entity directDamager, Entity damager, Projectile proj,
			ItemStack item, HashMap<DamageModifier, Double> defaultModifiers)
	{
		_damagee = damagee;
		_cause = cause;

		_directDamager = directDamager;
		_damager = damager;
		_proj = proj;

		_item = item;

		_defaultModifiers = defaultModifiers;
	}

	public Entity getDamagee()
	{
		return _damagee;
	}

	public DamageCause getCause()
	{
		return _cause;
	}

	public Entity getDirectDamager()
	{
		return _directDamager;
	}

	public Entity getDamager()
	{
		return _damager;
	}

	public Projectile getProjectile()
	{
		return _proj;
	}

	public HashMap<DamageModifier, Double> getModifiers()
	{
		return _defaultModifiers;
	}

	public double getDamage(DamageModifier modifier)
	{
		if (!_defaultModifiers.containsKey(modifier))
			return 0;

		return _defaultModifiers.get(modifier);
	}

	public void setDamage(DamageModifier modifier, double damage)
	{
		_defaultModifiers.put(modifier, damage);
	}

	public void setReduction(DamageModifier modifier, double damage)
	{
		setDamage(modifier, -damage);
	}

	public void setDamage(double damage)
	{
		ignoreAllModifiers();
		setDamage(DamageModifier.BASE, damage);
	}

	public double getOriginalDamage()
	{
		return getDamage(DamageModifier.BASE);
	}

	public void ignore(DamageModifier modifier)
	{
		_defaultModifiers.put(modifier, (double) 0);
	}

	public void ignoreAllModifiers()
	{
		for (DamageModifier modifier : DamageModifier.values())
		{
			if (modifier != DamageModifier.BASE)
				ignore(modifier);
		}
	}

	public double getReduction(DamageModifier modifier)
	{
		return -(getDamage(modifier));
	}

	public boolean isPlayer()
	{
		return getDamagee() instanceof Player;
	}

	public boolean isAttack()
	{
		return getDirectDamager() != null;
	}

	public boolean byPlayer()
	{
		return getDamager() instanceof Player;
	}

	public boolean hasProjectile()
	{
		return getProjectile() != null;
	}

	public ItemStack getItem()
	{
		return _item;
	}
}
