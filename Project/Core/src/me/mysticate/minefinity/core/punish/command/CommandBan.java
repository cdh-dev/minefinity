package me.mysticate.minefinity.core.punish.command;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.DataPlayerManager.OfflineDataCallback;
import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilTime;
import me.mysticate.minefinity.core.common.util.UtilTime.TimeUnit;
import me.mysticate.minefinity.core.punish.PunishManager;
import me.mysticate.minefinity.core.punish.PunishPlayer;
import me.mysticate.minefinity.core.rank.Rank;

public class CommandBan extends CommandBase<PunishManager>
{
	public CommandBan(CommandManager commandManager, PunishManager manager)
	{
		super(commandManager, manager, new String[] { "ban", "b", "gitrektdude" }, Rank.MOD,
				"<Player> <Time> <Reason>");
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		if (args.length < 3)
		{
			return false;
		}

		String playerName = args[0];

		String serializedTime = args[1].toLowerCase();
		String lastTime = "";
		long tt = 0L;
		for (int i = 0; i < serializedTime.length(); i++)
		{
			Character cur = serializedTime.charAt(i);

			try
			{
				if (cur == 'y')
				{
					tt += (TimeUnit.Year.getMillis() * Integer.valueOf(lastTime));
					lastTime = "";

					continue;
				} else if (cur == 'd')
				{
					tt += (TimeUnit.Day.getMillis() * Integer.valueOf(lastTime));
					lastTime = "";

					continue;
				} else if (cur == 'h')
				{
					tt += (TimeUnit.Hour.getMillis() * Integer.valueOf(lastTime));
					lastTime = "";

					continue;
				} else if (cur == 'm')
				{
					tt += (TimeUnit.Minute.getMillis() * Integer.valueOf(lastTime));
					lastTime = "";

					continue;
				} else
				{
					lastTime += cur;
				}
			} catch (Exception ex)
			{
				ex.printStackTrace();
				return false;
			}
		}

		String m = "";
		for (int i = 2; i < args.length; i++)
			m += (args[i] + (i == args.length - 1 ? "" : " "));

		final long time = tt;
		final String message = m;

		sendMessage(player, "Processing request...");

		try
		{
			if (!getManager().loadOfflineData(playerName, new OfflineDataCallback<PunishPlayer>()
			{
				@Override
				public void onLoad(PunishPlayer data, boolean offline)
				{
					data.addBan(player.getName(), message, time);

					sendMessage(player, "Banned player " + C.yellow + playerName + C.gray + " for " + C.red
							+ UtilTime.getLongestTime(time).display() + C.gray + ".");

					if (offline)
					{
						data.save();
					} else
					{
						getManager().enforceBans();
					}
				}
			}))
			{
				sendMessage(player, "Unknown player " + C.yellow + playerName + C.gray + ".");
			}
		} catch (Exception ex)
		{
			sendMessage(player, C.red + "An error occured while processing your request. Is that the correct name?");
		}

		return true;
	}
}
