package me.mysticate.minefinity.core.punish.gui;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.punish.PunishmentType;

public class ButtonPunishmentType implements Button
{
	private GuiPunishments _gui;
	private PunishmentType _type;
	private Material _mat;
	
	public ButtonPunishmentType(GuiPunishments gui, PunishmentType type, Material mat)
	{
		_gui = gui;
		_type = type;
		_mat = mat;
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		ItemStack stack = UtilItemStack.create(_mat, (byte) 0, 1, C.yellowBold + _type.toString().toUpperCase() + "S");
		
		if (_gui.getFocus() == _type)
			stack.addEnchantment(UtilItemStack.getGlow(), 1);
		
		return stack;
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		_gui.shiftFocus(_type);
	}
}
