package me.mysticate.minefinity.core.punish;

public enum PunishmentType
{
	Ban, Mute
}
