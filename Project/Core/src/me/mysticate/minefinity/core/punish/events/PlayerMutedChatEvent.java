package me.mysticate.minefinity.core.punish.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import me.mysticate.minefinity.core.chat.ChatEvent;

public class PlayerMutedChatEvent extends PlayerEvent
{
	private static HandlerList _handlers = new HandlerList();

	private static HandlerList getHandlerList()
	{
		return _handlers;
	}

	@Override
	public HandlerList getHandlers()
	{
		return getHandlerList();
	}

	private ChatEvent _event;

	public PlayerMutedChatEvent(Player who, ChatEvent event)
	{
		super(who);

		_event = event;
	}

	public ChatEvent getEvent()
	{
		return _event;
	}
}
