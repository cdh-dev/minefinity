package me.mysticate.minefinity.core.backpack;

public enum ItemPool
{
	HUB, BATTLE_ARENA;
	
	public static ItemPool getByName(String name)
	{
		for (ItemPool type : values())
		{
			if (type.toString().equalsIgnoreCase(name))
				return type;
		}

		return null;
	}
}
