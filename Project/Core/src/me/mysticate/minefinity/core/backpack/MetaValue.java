package me.mysticate.minefinity.core.backpack;

public class MetaValue
{
	private String _value;

	public MetaValue(String value)
	{
		_value = value;
	}

	public boolean isInt()
	{
		return asInt() != null;
	}

	public Integer asInt()
	{
		try
		{
			return Integer.valueOf(_value);
		} catch (Exception ex)
		{
			return null;
		}
	}

	public boolean isString()
	{
		return !isInt();
	}

	public String asString()
	{
		return _value;
	}
}
