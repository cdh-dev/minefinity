package me.mysticate.minefinity.core.backpack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import me.mysticate.minefinity.core.IM;
import me.mysticate.minefinity.core.file.DataFolder;
import me.mysticate.minefinity.core.file.YamlF;
import me.mysticate.minefinity.core.player.DataPlayer;

public class BackpackPlayer extends DataPlayer
{
	private List<AquiredItem> _items = new ArrayList<AquiredItem>();

	public BackpackPlayer(UUID id)
	{
		super(id, DataFolder.getBackpack(id));
	}

	public AquiredItem addItem(String id, ItemPool pool, HashMap<String, MetaValue> meta)
	{
		UUID uuid = IM.backpack().nextUUID();
		IM.backpack().addUUID(uuid);

		AquiredItem item = new AquiredItem(uuid, id, pool, meta);
		_items.add(item);
		return item;
	}

	public boolean removeItem(AquiredItem item)
	{
		return _items.remove(item);
	}

	public List<AquiredItem> getItems(String name, ItemPool pool)
	{
		List<AquiredItem> items = new ArrayList<AquiredItem>();

		for (AquiredItem item : _items)
		{
			if (item.getId().equals(name) && item.getPool() == pool)
			{
				items.add(item);
			}
		}
		return items;
	}

	public List<AquiredItem> getItems(ItemPool pool)
	{
		List<AquiredItem> items = new ArrayList<AquiredItem>();

		for (AquiredItem item : _items)
		{
			if (item.getPool() == pool)
			{
				items.add(item);
			}
		}
		return items;
	}

	public List<AquiredItem> getItems(String name)
	{
		List<AquiredItem> items = new ArrayList<AquiredItem>();

		for (AquiredItem item : _items)
		{
			if (item.getId().equals(name))
			{
				items.add(item);
			}
		}
		return items;
	}
	
	public AquiredItem getItem(UUID uuid)
	{
		for (AquiredItem item : _items)
		{
			if (item.getUUID().equals(uuid))
				return item;
		}
		return null;
	}

	@Override
	public void load()
	{
		for (String pool : readLines("items"))
		{
			ItemPool itemPool = null;

			try
			{
				itemPool = ItemPool.valueOf(pool);
			} catch (Exception ex)
			{
				ex.printStackTrace();
			}

			if (itemPool == null)
				continue;

			for (String name : readLines(YamlF.path("items", pool)))
			{
				for (String uuid : readLines(YamlF.path("items", pool, name)))
				{
					UUID id = null;

					try
					{
						id = UUID.fromString(uuid);
					} catch (Exception ex)
					{
						ex.printStackTrace();
					}

					if (id == null)
						continue;

					if (!IM.backpack().hasUUID(id))
						continue;

					HashMap<String, MetaValue> meta = new HashMap<String, MetaValue>();

					for (String key : readLines(YamlF.path("items", pool, name, uuid)))
					{
						String value = read(YamlF.path("items", pool, name, uuid, key), null);
						if (value == null)
							continue;

						meta.put(key, new MetaValue(value));
					}

					_items.add(new AquiredItem(id, name, itemPool, meta));
				}
			}
		}
	}

	@Override
	public void save()
	{
		if (!_items.isEmpty())
			write("", "items");

		for (AquiredItem item : _items)
		{
			for (String key : item.getMeta().keySet())
			{
				write(item.getValue(key).asString(),
						YamlF.path("items", item.getPool().toString(), item.getId(), item.getUUID(), key));
			}
		}
	}
}
