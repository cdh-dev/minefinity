package me.mysticate.minefinity.core.game;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class RequestPlayerOpenGameJoinerEvent extends PlayerEvent
{
	private static HandlerList _handlers = new HandlerList();

	private static HandlerList getHandlerList()
	{
		return _handlers;
	}

	@Override
	public HandlerList getHandlers()
	{
		return getHandlerList();
	}

	private String _game;

	public RequestPlayerOpenGameJoinerEvent(Player who, String game)
	{
		super(who);

		_game = game;
	}

	public String getGame()
	{
		return _game;
	}
}
