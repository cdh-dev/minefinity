package me.mysticate.minefinity;

import org.bukkit.plugin.java.JavaPlugin;

public class MinefinityPlugin
{
	private static JavaPlugin _plugin = null;
	private static String _website = "www.minefinity.com";

	public static void initialize(JavaPlugin plugin)
	{
		if (_plugin != null)
			throw new IllegalArgumentException("MinefinityPlugin can only be initialized once!");

		_plugin = plugin;
	}

	public static JavaPlugin getPlugin()
	{
		return _plugin;
	}

	public static String getWebsite()
	{
		return _website;
	}
}
