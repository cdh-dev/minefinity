package me.mysticate.minefinity.game;

import org.bukkit.World;
import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.sector.ISectorHandler;
import me.mysticate.minefinity.core.sector.Sector;
import me.mysticate.minefinity.core.sector.SectorManager;
import me.mysticate.minefinity.core.sector.SectorWorld;

public abstract class GameSectorHandler implements ISectorHandler
{
	private GameManager _manager;
	private GameType _game;

	public GameSectorHandler(GameManager manager, GameType game)
	{
		_manager = manager;
		_game = game;
	}

	@Override
	public SectorManager getSectorManager()
	{
		return _manager.getSectorManager();
	}

	@Override
	public Sector getSector()
	{
		return _game.getSector();
	}

	@Override
	public void onSectorWorldChange(Player player, World to, World from)
	{
		Game game = _manager.getGame(player, _game);
		if (game == null)
			return;

		game.sendMessage(C.redBold + "- " + _manager.getRankManager().getPlayer(player).getDominant().getColor()
				+ player.getName());

		game.removePlayer(player);
		game.onQuit(player);
	}

	@Override
	public void onWorldJoinSector(SectorWorld sector)
	{
		System.out.println("Loaded world " + sector.getWorld().getName() + " into GameType:" + _game.getAbv());
	}

	@Override
	public void onWorldExitSector(SectorWorld sector)
	{
		System.out.println("Unloaded world " + sector.getWorld().getName() + " from GameType:" + _game.getAbv());
	}

	@Override
	public void onSectorJoin(Player player)
	{
		Game game = _manager.getGame(player, _game);
		if (game == null)
			return;

		game.sendMessage(C.greenBold + "+ " + _manager.getRankManager().getPlayer(player).getDominant().getColor()
				+ player.getName());

		game.addPlayer(player);
		game.onJoin(player);

		game.giveLobbyItems(player);
	}

	@Override
	public void onSectorQuit(Player player)
	{
		Game game = _manager.getGame(player, _game);
		if (game == null)
			return;

		game.sendMessage(C.redBold + "- " + _manager.getRankManager().getPlayer(player).getDominant().getColor()
				+ player.getName());

		game.removePlayer(player);
		game.onQuit(player);
	}
}
