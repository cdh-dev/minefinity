package me.mysticate.minefinity.game.commands;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.rank.Rank;
import me.mysticate.minefinity.game.Game;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.GameState;

public class CommandEnd extends CommandBase<GameManager>
{
	public CommandEnd(CommandManager commandManager, GameManager manager)
	{
		super(commandManager, manager, new String[] { "end" }, Rank.ADMIN, null);

		setInvisible(true);
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		Game game = getManager().getGame(player);
		if (game == null)
			return false;

		if (game.getState() != GameState.INGAME)
		{
			sendMessage(player, "The game hasn't started yet!");
			return true;
		}

		game.sendMessage(F.manager(game.getName(), C.yellow + player.getName() + C.gray + " has ended the game!"));
		game.setState(GameState.POSTGAME);
		return true;
	}
}
