package me.mysticate.minefinity.game.commands;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.command.CommandBase;
import me.mysticate.minefinity.core.command.CommandManager;
import me.mysticate.minefinity.core.rank.Rank;
import me.mysticate.minefinity.game.Game;
import me.mysticate.minefinity.game.GameManager;

public class CommandHub extends CommandBase<GameManager>
{
	public CommandHub(CommandManager commandManager, GameManager manager)
	{
		super(commandManager, manager, new String[] { "hub", "leave", "quit", "lobby" }, Rank.DEFAULT, null);

		setInvisible(true);
	}

	@Override
	public boolean execute(Player player, String alias, String[] args)
	{
		Game game = getManager().getGame(player);
		if (game == null)
			return false;

		sendMessage(player, "Connecting you to a hub...");
		game.sendToHub(player);

		return true;
	}
}
