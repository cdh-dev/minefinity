package me.mysticate.minefinity.game.data;

import java.util.HashMap;

import me.mysticate.minefinity.core.currency.CurrencyType;

public class PlayerRewardSummary
{
	private HashMap<CurrencyType, Integer> _rewards = new HashMap<CurrencyType, Integer>();

	public int getRewards(CurrencyType type)
	{
		if (!_rewards.containsKey(type))
			return 0;

		return _rewards.get(type);
	}

	public void addRewards(CurrencyType type, int amount)
	{
		_rewards.put(type, (_rewards.containsKey(type) ? _rewards.get(type) : 0) + amount);
	}
}
