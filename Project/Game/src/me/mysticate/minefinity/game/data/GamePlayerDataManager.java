package me.mysticate.minefinity.game.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.PlayerManager;
import me.mysticate.minefinity.game.GameType;

public class GamePlayerDataManager extends PlayerManager<ScorePlayer>
{
	public static class ScoreWrapper implements Comparable<ScoreWrapper>
	{
		public Player Player;
		public Score Score;

		@Override
		public int compareTo(ScoreWrapper o)
		{
			return Score.compareTo(o.Score);
		}
	}

	public GamePlayerDataManager(JavaPlugin plugin)
	{
		super(plugin, "Game Scoring");
	}

	public List<ScoreWrapper> getOrderedScores(GameType game, String key, Collection<Player> players)
	{
		ArrayList<ScoreWrapper> ordered = new ArrayList<ScoreWrapper>();
		for (Player player : players)
		{
			ScorePlayer scored = getPlayer(player);
			Score score = scored.getScore(game, key);
			if (score != null)
			{
				ScoreWrapper wrapper = new ScoreWrapper();
				wrapper.Player = player;
				wrapper.Score = score;

				ordered.add(wrapper);
			}
		}

		Collections.sort(ordered);
		return ordered;
	}

	@Override
	protected ScorePlayer newData(Player player)
	{
		return new ScorePlayer(player.getUniqueId());
	}
}
