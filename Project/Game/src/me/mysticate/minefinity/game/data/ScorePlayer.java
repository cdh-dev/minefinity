package me.mysticate.minefinity.game.data;

import java.util.HashMap;
import java.util.UUID;

import me.mysticate.minefinity.core.player.PlayerData;
import me.mysticate.minefinity.game.GameType;

public class ScorePlayer extends PlayerData
{
	private HashMap<GameType, HashMap<String, Score>> _scores = new HashMap<GameType, HashMap<String, Score>>();

	public ScorePlayer(UUID id)
	{
		super(id);
	}

	public HashMap<String, Score> getAllScores(GameType game)
	{
		return _scores.get(game);
	}

	public Score getScore(GameType game, String name)
	{
		if (!_scores.containsKey(game))
			_scores.put(game, new HashMap<String, Score>());

		return getAllScores(game).get(name);
	}

	public void setScore(GameType game, String name, double value)
	{
		Score score = getScore(game, name);
		if (score == null)
		{
			_scores.get(game).put(name, new Score(value));
		} else
		{
			score.set(value);
		}
	}

	public void addScore(GameType game, String name, double value)
	{
		Score score = getScore(game, name);
		if (score == null)
		{
			_scores.get(game).put(name, new Score(value));
		} else
		{
			score.add(value);
		}
	}

	public void removeScores(GameType game)
	{
		_scores.put(game, new HashMap<String, Score>());
	}

	public void removeScore(GameType game, String name, double value)
	{
		Score score = getScore(game, name);
		if (score == null)
		{
			_scores.get(game).put(name, new Score(-value));
		} else
		{
			score.subtract(value);
		}
	}

	public void removeScore(GameType game, String name)
	{
		if (!_scores.containsKey(game))
			_scores.put(game, new HashMap<String, Score>());

		getAllScores(game).remove(name);
	}
}
