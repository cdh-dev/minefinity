package me.mysticate.minefinity.game.games.battlearena.kits.knight.levels;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.game.games.battlearena.BattleArena;
import me.mysticate.minefinity.game.games.battlearena.kits.knight.KitKnight;
import me.mysticate.minefinity.game.kit.kit.ILoadoutLevel;

public class Knight2 extends ILoadoutLevel<BattleArena, KitKnight>
{
	public Knight2(BattleArena game, KitKnight kit)
	{
		super(game, kit);
	}

	@Override
	public void giveItems()
	{
		String swordName = getSetting("name-sword");
		String bowName = getSetting("name-bow");

		ItemStack sword = UtilItemStack.create(Material.WOOD_SWORD, (byte) 0, 1, C.green + "Wooden Sword");
		UtilItemStack.unbreakable(sword);

		ItemStack bow = UtilItemStack.create(Material.BOW, (byte) 0, 1, C.green + "Basic Longbow");
		UtilItemStack.unbreakable(bow);

		ItemStack boots = UtilItemStack.create(Material.GOLD_BOOTS, (byte) 0, 1, C.green + "Golden Boots");
		UtilItemStack.unbreakable(boots);

		ItemStack chestplate = UtilItemStack.create(Material.LEATHER_CHESTPLATE, (byte) 0, 1,
				C.green + "Leather Chestplate");
		UtilItemStack.unbreakable(chestplate);

		if (swordName != null)
			UtilItemStack.name(sword, swordName);

		if (bowName != null)
			UtilItemStack.name(bow, bowName);

		setItem(0, sword);
		setItem(1, bow);
		setItem(9, new ItemStack(Material.ARROW, 2));

		setArmor(0, boots);
		setArmor(2, chestplate);
	}
}
