package me.mysticate.minefinity.game.games.battlearena.scoreboard;

import java.util.List;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilTime;
import me.mysticate.minefinity.core.hud.HudManager;
import me.mysticate.minefinity.core.updater.event.TimeEvent;
import me.mysticate.minefinity.game.IScoreboardHandler;
import me.mysticate.minefinity.game.data.GamePlayerDataManager.ScoreWrapper;
import me.mysticate.minefinity.game.games.battlearena.BattleArena;

public class BAScoreboardPostGame extends IScoreboardHandler<BattleArena>
{
	public BAScoreboardPostGame(BattleArena game)
	{
		super(game);
	}

	@Override
	public void update(TimeEvent event, Player player)
	{
		HudManager.get(player).scoreboardAddBlank();
		HudManager.get(player).scoreboardAddText(C.goldBold + "Postgame");
		HudManager.get(player)
				.scoreboardAddText(C.white + UtilTime.fromSecsToString((int) (24 - getGame().getStateCount())));
		HudManager.get(player).scoreboardAddBlank();

		HudManager.get(player).scoreboardAddText(C.goldBold + "Top Scores");

		List<ScoreWrapper> scores = getGame().getManager().getScoreManager().getOrderedScores(getGame().getType(),
				"kills", getGame().getPlayers());

		for (int i = 0; i < scores.size() && i < 5; i++)
		{
			ScoreWrapper wrapper = scores.get(i);
			HudManager.get(player).scoreboardAddText(C.green + "#" + (i + 1) + C.gray + " - "
					+ getGame().formatName(wrapper.Player) + C.gray + " - " + C.aqua + ((int) wrapper.Score.value()));
		}

		int playerScore = -1;
		for (int i = 1; i <= scores.size(); i++)
			if (scores.get(i - 1).Player == player)
				playerScore = i;

		if (playerScore != -1)
		{
			HudManager.get(player).scoreboardAddBlank();
			HudManager.get(player).scoreboardAddText(C.yellowBold + "Position " + C.white + "#" + playerScore);
		}

		HudManager.get(player).scoreboardBuild();
	}
}
