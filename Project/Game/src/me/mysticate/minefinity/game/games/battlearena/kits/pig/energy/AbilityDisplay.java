package me.mysticate.minefinity.game.games.battlearena.kits.pig.energy;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.IM;
import me.mysticate.minefinity.core.cooldown.CooldownPool;
import me.mysticate.minefinity.core.energy.IEnergyDisplay;
import me.mysticate.minefinity.game.Game;

public class AbilityDisplay implements IEnergyDisplay
{
	private Player _player;
	private Game _game;

	public AbilityDisplay(Player player, Game game)
	{
		_player = player;
		_game = game;
	}

	@Override
	public float getExpValue()
	{
		if (!_game.inGame())
			return 0F;

		if (IM.cooldown().getPlayer(_player).hasCooldown(CooldownPool.BATTLE_ARENA, "Piggy-Throw"))
			return IM.cooldown().getPlayer(_player).getCooldown(CooldownPool.BATTLE_ARENA, "Piggy-Throw").asFloat();

		return .999F;
	}

	@Override
	public boolean shouldFlashWhenFull()
	{
		return true;
	}
}
