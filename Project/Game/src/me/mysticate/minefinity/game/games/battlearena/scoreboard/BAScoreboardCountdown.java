package me.mysticate.minefinity.game.games.battlearena.scoreboard;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.hud.HudManager;
import me.mysticate.minefinity.core.updater.event.TimeEvent;
import me.mysticate.minefinity.game.IScoreboardHandler;
import me.mysticate.minefinity.game.games.battlearena.BattleArena;
import me.mysticate.minefinity.game.kit.kit.Kit;

public class BAScoreboardCountdown extends IScoreboardHandler<BattleArena>
{
	public BAScoreboardCountdown(BattleArena game)
	{
		super(game);
	}

	@Override
	public void update(TimeEvent event, Player player)
	{
		Kit<?> kit = getGame().getKit(player);

		HudManager.get(player).scoreboardAddText("           " + C.white + getGame().getPlayerCount() + C.gray + " / "
				+ C.white + getGame().getType().getMax());
		HudManager.get(player).scoreboardAddBlank();
		HudManager.get(player).scoreboardAddText(C.goldBold + "Selected Kit");
		HudManager.get(player).scoreboardAddText(C.white + kit.getDisplayName());
		HudManager.get(player).scoreboardAddBlank();
		HudManager.get(player).scoreboardAddText(C.goldBold + "Level");
		HudManager.get(player).scoreboardAddText(C.aqua + "Kit " + C.white + kit.getLevel());
		HudManager.get(player).scoreboardAddText(C.aqua + "Skill " + C.white + kit.getLevel(Kit.Magic));
		HudManager.get(player).scoreboardAddBlank();
		HudManager.get(player).scoreboardAddText(C.yellowBold + "Starting in");
		HudManager.get(player).scoreboardAddText(C.white + getGame().getTimeBeforeLobbyEnd() + " Second"
				+ (getGame().getTimeBeforeLobbyEnd() == 1 ? "" : "s"));

		HudManager.get(player).scoreboardBuild();

		HudManager.get(player).setPrefix("");
		HudManager.get(player).setSuffix("");
	}
}
