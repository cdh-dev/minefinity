package me.mysticate.minefinity.game.games.battlearena.kits.sniper.levels;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.game.games.battlearena.BattleArena;
import me.mysticate.minefinity.game.games.battlearena.kits.sniper.KitSniper;
import me.mysticate.minefinity.game.kit.kit.ILoadoutLevel;

public class Sniper5 extends ILoadoutLevel<BattleArena, KitSniper>
{
	public Sniper5(BattleArena game, KitSniper kit)
	{
		super(game, kit);
	}

	@Override
	public void giveItems()
	{
		String axeName = getSetting("name-axe");
		String bowName = getSetting("name-bow");

		ItemStack axe = UtilItemStack.create(Material.STONE_AXE, (byte) 0, 1, C.green + "Forged War Axe");
		UtilItemStack.unbreakable(axe);

		ItemStack bow = UtilItemStack.create(Material.BOW, (byte) 0, 1, C.green + "Reinforced Tension Bow");
		UtilItemStack.unbreakable(bow);

		bow.addEnchantment(Enchantment.ARROW_DAMAGE, 2);

		if (getKit().getMagic() > 0)
		{
			UtilItemStack.setLore(bow,
					new String[] { " ",
							C.yellow + "Long-range" + C.white + " for " + C.green + "Headshot" + C.white + ".", " ",
							C.gray + "Shooting an enemy with",
							C.gray + "your bow from " + C.yellow + getKit().getDistance() + "+ Blocks",
							C.gray + "away will deal crippling damage.", });
		}

		ItemStack boots = UtilItemStack.create(Material.IRON_BOOTS, (byte) 0, 1, C.green + "Iron Boots");
		UtilItemStack.unbreakable(boots);

		ItemStack leggings = UtilItemStack.create(Material.CHAINMAIL_LEGGINGS, (byte) 0, 1,
				C.green + "Chainmail Leggings");
		UtilItemStack.unbreakable(leggings);

		ItemStack chestplate = UtilItemStack.create(Material.CHAINMAIL_CHESTPLATE, (byte) 0, 1,
				C.green + "Chainmail Chestplate");
		UtilItemStack.unbreakable(chestplate);

		ItemStack helmet = UtilItemStack.create(Material.IRON_HELMET, (byte) 0, 1, C.green + "Iron Helmet");
		UtilItemStack.unbreakable(helmet);

		if (axeName != null)
			UtilItemStack.name(axe, C.goldBold + axeName);

		if (bowName != null)
			UtilItemStack.name(bow, C.goldBold + bowName);

		setItem(0, bow);
		setItem(1, axe);
		setItem(9, new ItemStack(Material.ARROW, 64));

		setArmor(0, boots);
		setArmor(1, leggings);
		setArmor(2, chestplate);
		setArmor(3, helmet);
	}
}
