package me.mysticate.minefinity.game.games.battlearena.kits.pig.levels;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.game.games.battlearena.BattleArena;
import me.mysticate.minefinity.game.games.battlearena.kits.pig.KitPig;
import me.mysticate.minefinity.game.kit.kit.ILoadoutLevel;

public class Pig4 extends ILoadoutLevel<BattleArena, KitPig>
{
	public Pig4(BattleArena game, KitPig kit)
	{
		super(game, kit);
	}

	@Override
	public void giveItems()
	{
		ItemStack boots = UtilItemStack.create(Material.DIAMOND_BOOTS, (byte) 0, 1, C.green + "Diamond Boots");
		UtilItemStack.unbreakable(boots);

		ItemStack leggings = UtilItemStack.create(Material.LEATHER_LEGGINGS, (byte) 0, 1,
				C.green + "Leather Chestplate");
		UtilItemStack.unbreakable(leggings);
		UtilItemStack.color(leggings, Color.FUCHSIA);

		ItemStack chestplate = UtilItemStack.create(Material.LEATHER_CHESTPLATE, (byte) 0, 1,
				C.green + "Leather Chestplate");
		UtilItemStack.unbreakable(chestplate);
		UtilItemStack.color(chestplate, Color.FUCHSIA);

		ItemStack helmet = UtilItemStack.create(Material.IRON_HELMET, (byte) 0, 1, C.green + "Iron Helmet");
		UtilItemStack.unbreakable(helmet);

		setArmor(0, boots);
		setArmor(1, leggings);
		setArmor(2, chestplate);
		setArmor(3, helmet);
	}
}
