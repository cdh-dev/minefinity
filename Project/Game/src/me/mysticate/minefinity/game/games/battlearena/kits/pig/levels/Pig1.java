package me.mysticate.minefinity.game.games.battlearena.kits.pig.levels;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.game.games.battlearena.BattleArena;
import me.mysticate.minefinity.game.games.battlearena.kits.pig.KitPig;
import me.mysticate.minefinity.game.kit.kit.ILoadoutLevel;

public class Pig1 extends ILoadoutLevel<BattleArena, KitPig>
{
	public Pig1(BattleArena game, KitPig kit)
	{
		super(game, kit);
	}

	@Override
	public void giveItems()
	{
		ItemStack boots = UtilItemStack.create(Material.IRON_BOOTS, (byte) 0, 1, C.green + "Iron Boots");
		UtilItemStack.unbreakable(boots);

		ItemStack helmet = UtilItemStack.create(Material.IRON_HELMET, (byte) 0, 1, C.green + "Iron Helmet");
		UtilItemStack.unbreakable(helmet);

		setArmor(0, boots);
		setArmor(3, helmet);
	}
}
