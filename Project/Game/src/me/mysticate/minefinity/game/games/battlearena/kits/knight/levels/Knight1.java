package me.mysticate.minefinity.game.games.battlearena.kits.knight.levels;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.game.games.battlearena.BattleArena;
import me.mysticate.minefinity.game.games.battlearena.kits.knight.KitKnight;
import me.mysticate.minefinity.game.kit.kit.ILoadoutLevel;

public class Knight1 extends ILoadoutLevel<BattleArena, KitKnight>
{
	public Knight1(BattleArena game, KitKnight kit)
	{
		super(game, kit);
	}

	@Override
	public void giveItems()
	{
		String name = getSetting("name-sword");

		ItemStack sword = UtilItemStack.create(Material.WOOD_SWORD, (byte) 0, 1, C.green + "Wooden Sword");
		UtilItemStack.unbreakable(sword);

		ItemStack boots = UtilItemStack.create(Material.GOLD_BOOTS, (byte) 0, 1, C.green + "Golden Boots");
		UtilItemStack.unbreakable(boots);

		if (name != null)
			UtilItemStack.name(sword, C.goldBold + name);

		setItem(0, sword);
		setArmor(0, boots);
	}
}
