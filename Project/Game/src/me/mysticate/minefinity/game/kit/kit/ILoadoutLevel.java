package me.mysticate.minefinity.game.kit.kit;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.game.Game;

public abstract class ILoadoutLevel<G extends Game, K extends Kit<G>>
{
	private G _game;
	private K _kit;

	public ILoadoutLevel(G game, K kit)
	{
		_game = game;
		_kit = kit;
	}

	public G getGame()
	{
		return _game;
	}

	public K getKit()
	{
		return _kit;
	}

	public Player getPlayer()
	{
		return _kit.getPlayer();
	}

	protected void setItem(int slot, ItemStack item)
	{
		getPlayer().getInventory().setItem(slot, item);
	}

	protected void addItem(ItemStack item)
	{
		getPlayer().getInventory().addItem(item);
	}

	protected void setArmor(int slot, ItemStack item)
	{
		ItemStack[] items = getPlayer().getInventory().getArmorContents();
		if (items == null)
			items = new ItemStack[4];

		items[slot] = item;
		getPlayer().getInventory().setArmorContents(items);
	}

	public boolean hasSetting(String setting)
	{
		return getKit().hasSetting(setting);
	}

	public String getSetting(String setting)
	{
		return getKit().getSetting(setting);
	}

	public abstract void giveItems();
}
