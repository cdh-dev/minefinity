package me.mysticate.minefinity.game.kit.kit;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.common.util.UtilInv;
import me.mysticate.minefinity.game.Game;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.GameType;

public abstract class Kit<G extends Game> implements Listener
{
	public static final String Level = "Level";
	public static final String Magic = "Magic";

	public static enum KitDisableReason
	{
		SWITCH_KITS, END_GAME, QUIT
	}

	public static String getPerkName(GameType game, int kit, String key)
	{
		return F.perk(game.getAbv(), kit + "", key);
	}

	private GameManager _manager;
	private G _game;

	private Player _player;

	private IKit<G> _housing;

	private HashMap<Integer, ILoadoutLevel<G, ? extends Kit<G>>> _levelLoadouts = new HashMap<Integer, ILoadoutLevel<G, ? extends Kit<G>>>();

	public Kit(GameManager manager, G game, Player player, IKit<G> housing)
	{
		_manager = manager;
		_game = game;

		_player = player;

		_housing = housing;
	}

	public GameManager getManager()
	{
		return _manager;
	}

	public G getGame()
	{
		return _game;
	}

	public Player getPlayer()
	{
		return _player;
	}

	public IKit<G> getKit()
	{
		return _housing;
	}

	public int getId()
	{
		return getKit().getId();
	}

	public String getDisplayName()
	{
		return getKit().getDisplayName();
	}

	public void setLoadout(int level, ILoadoutLevel<G, ? extends Kit<G>> loadout)
	{
		_levelLoadouts.put(level, loadout);
	}

	public abstract void onEnable();

	public abstract void onDisable(KitDisableReason reason);

	public void giveItems()
	{
		UtilInv.clearInvAbs(getPlayer());

		int level = getLevel();
		if (_levelLoadouts.containsKey(level))
		{
			_levelLoadouts.get(level).giveItems();
		} else
		{
			for (int i = level; i >= 0; i--) // Choose next highest
			{
				if (_levelLoadouts.containsKey(i))
				{
					_levelLoadouts.get(i).giveItems();
					break;
				}
			}
		}

		giveItemsCustom();
	}

	protected abstract void giveItemsCustom();

	public boolean hasLevel(String key, int level)
	{
		return getLevel(key) >= level;
	}

	public int getLevel()
	{
		return getLevel(Kit.Level);
	}

	public int getMagic()
	{
		return getLevel(Kit.Magic);
	}

	public int getLevel(String key)
	{
		return getManager().getPerkManager().getPlayer(getPlayer())
				.getLevel(getPerkName(getGame().getType(), getId(), key));
	}

	public String getKeyIdentifier()
	{
		return getGame().getType().getAbv() + "-" + getDisplayName();
	}

	public boolean hasSetting(String setting)
	{
		return getGame().getManager().getSettingsManager().getPlayer(getPlayer())
				.hasSetting(getGame().getType().getSettings(), setting);
	}

	public String getSetting(String setting)
	{
		if (!hasSetting(setting))
			return null;

		return getGame().getManager().getSettingsManager().getPlayer(getPlayer())
				.getSetting(getGame().getType().getSettings(), setting);
	}

	public String recharge(String name)
	{
		return getDisplayName() + "-" + name;
	}
}