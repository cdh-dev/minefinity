package me.mysticate.minefinity.game.kit.kit;

import org.bukkit.entity.Player;

import me.mysticate.minefinity.game.Game;
import me.mysticate.minefinity.game.GameManager;

public interface IKitBuilder<G extends Game>
{
	public Kit<G> newInstance(GameManager manager, G game, Player player);
}
