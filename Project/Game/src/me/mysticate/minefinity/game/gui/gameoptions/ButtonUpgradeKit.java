package me.mysticate.minefinity.game.gui.gameoptions;

import java.util.LinkedList;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.text.F;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.currency.CurrencyPlayer;
import me.mysticate.minefinity.core.currency.CurrencyType;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.gui.confirmation.ConfirmationPage;
import me.mysticate.minefinity.core.gui.confirmation.ConfirmationResponse;
import me.mysticate.minefinity.core.gui.confirmation.IConfirmationCallback;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.GameType;
import me.mysticate.minefinity.game.kit.kit.IKit;
import me.mysticate.minefinity.game.kit.kit.Kit;

public class ButtonUpgradeKit implements Button
{
	private GameManager _manager;
	private GameType _game;
	private IKit<?> _kit;

	public ButtonUpgradeKit(GameManager manager, GameType game, IKit<?> kit)
	{
		_manager = manager;
		_game = game;
		_kit = kit;
	}

	private boolean owns(Player player)
	{
		return _kit.isFree()
				|| _manager.getPerkManager().getPlayer(player).hasPerk(Kit.getPerkName(_game, _kit.getId(), Kit.Level));
	}

	private int level(Player player)
	{
		if (!owns(player))
			return 0;

		return Math.max(1,
				_manager.getPerkManager().getPlayer(player).getLevel(Kit.getPerkName(_game, _kit.getId(), Kit.Level)));
	}

	private int magic(Player player)
	{
		return _manager.getPerkManager().getPlayer(player).getLevel(Kit.getPerkName(_game, _kit.getId(), Kit.Magic));
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		CurrencyPlayer currencyPlayer = _manager.getCurrencyManager().getPlayer(player);
		int level = level(player);
		int magic = magic(player);

		LinkedList<String> lore = new LinkedList<String>();
		lore.add(level > 0 ? (C.green + "Unlocked") : (C.red + "Locked"));
		lore.add(" ");

		for (String desc : _kit.getDisplayLore())
			lore.add(C.gray + desc);

		lore.add(" ");
		lore.add(C.aqua + "Level " + C.white + ((level > 0) ? level : "-"));

		if (level > 0 && magic > 0)
			lore.add(C.aqua + "Skill Level " + C.white + magic);

		final int levelCost = (int) (_kit.getUnlockCost() + ((_kit.getUnlockCost() + 250) * 1.5 * (level + 1)));
		final int magicCost = (int) ((_kit.getUnlockCost() + 250) * .2 * (level + 1));

		if (level > 0)
		{
			if (level < _kit.getMaxLevel())
			{
				lore.add(" ");

				for (String string : F.cost("+1 Level", -1, CurrencyType.IRON, levelCost,
						currencyPlayer.getCurrency(CurrencyType.IRON)))
					lore.add(string);
			}

			if (magic < _kit.getMaxMagic() && level >= _kit.getMagicPurchaseStart())
			{
				lore.add(" ");

				for (String string : F.cost("+1 Skill Level", 1, CurrencyType.GOLD, magicCost,
						currencyPlayer.getCurrency(CurrencyType.GOLD)))
					lore.add(string);
			}
		} else
		{
			lore.add(" ");

			for (String string : F.cost(_kit.getDisplayName(), 0, CurrencyType.IRON, _kit.getUnlockCost(),
					currencyPlayer.getCurrency(CurrencyType.IRON)))
				lore.add(string);
		}

		return UtilItemStack.create(_kit.getDisplayMat(), _kit.getDisplayData(), 1,
				C.yellowBold + _kit.getDisplayName(), lore.toArray(new String[0]));
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		CurrencyPlayer currencyPlayer = _manager.getCurrencyManager().getPlayer(player);
		int level = level(player);
		int magic = magic(player);

		final int levelCost = (int) (_kit.getUnlockCost() + ((_kit.getUnlockCost() + 250) * 1.5 * (level + 1)));
		final int magicCost = (int) ((_kit.getUnlockCost() + 250) * .2 * (level + 1));

		if (type == ClickType.LEFT)
		{
			if (level > 0)
			{
				if (level < _kit.getMaxLevel())
				{
					if (currencyPlayer.canAfford(CurrencyType.IRON, levelCost))
					{
						new ConfirmationPage(gui.getPlugin(), player, new IConfirmationCallback()
						{
							@Override
							public void run(ConfirmationResponse response)
							{
								if (response == ConfirmationResponse.CONFIRM)
								{
									if (currencyPlayer.removeCurrency(CurrencyType.IRON, levelCost))
									{
										_manager.getPerkManager().getPlayer(player)
												.setLevel(Kit.getPerkName(_game, _kit.getId(), Kit.Level), level + 1);
									} else
									{
										player.sendMessage(
												F.manager("Shop", "You don't have enough money to purchase that!"));
									}
								}

								gui.openInventory();
							}
						}, _kit.getDisplayName() + " Level " + (level + 1),
								levelCost + " " + CurrencyType.IRON.getName(), false).openInventory();
					}
				}
			} else
			{
				if (currencyPlayer.canAfford(CurrencyType.IRON, _kit.getUnlockCost()))
				{
					new ConfirmationPage(gui.getPlugin(), player, new IConfirmationCallback()
					{
						@Override
						public void run(ConfirmationResponse response)
						{
							if (response == ConfirmationResponse.CONFIRM)
							{
								currencyPlayer.removeCurrency(CurrencyType.IRON, _kit.getUnlockCost());
								_manager.getPerkManager().getPlayer(player)
										.setLevel(Kit.getPerkName(_game, _kit.getId(), Kit.Level), level + 1);
							}

							gui.openInventory();
						}
					}, _kit.getDisplayName() + " Level " + (level + 1),
							_kit.getUnlockCost() + " " + CurrencyType.IRON.getName(), false).openInventory();
				}
			}
		} else if (type == ClickType.RIGHT)
		{
			if (level > 0 && level >= _kit.getMagicPurchaseStart() && magic < _kit.getMaxMagic())
			{
				if (currencyPlayer.canAfford(CurrencyType.GOLD, magicCost))
				{
					new ConfirmationPage(gui.getPlugin(), player, new IConfirmationCallback()
					{
						@Override
						public void run(ConfirmationResponse response)
						{
							if (response == ConfirmationResponse.CONFIRM)
							{
								currencyPlayer.removeCurrency(CurrencyType.GOLD, magicCost);
								_manager.getPerkManager().getPlayer(player)
										.buyPerk(Kit.getPerkName(_game, _kit.getId(), Kit.Magic));
							}

							gui.openInventory();
						}
					}, _kit.getDisplayName() + " Skill Level " + (magic + 1),
							magicCost + " " + CurrencyType.GOLD.getName(), false).openInventory();
				}
			}
		}
	}
}