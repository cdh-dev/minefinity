package me.mysticate.minefinity.game.gui.gameoptions.battlearena;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.gui.gameoptions.battlearena.rename.GuiBAItemRename;

public class ButtonOptionsRenameItem implements Button
{
	private GameManager _manager;

	public ButtonOptionsRenameItem(GameManager manager)
	{
		_manager = manager;
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		return UtilItemStack.create(Material.NAME_TAG, (byte) 0, 1, C.yellowBold + "Rename Items",
				new String[] { " ", C.gray + "Add your personal flair to", C.gray + "the game by renaming your",
						C.gray + "weapons!", " ", C.white + "Renames apply to all weapons",
						C.white + "of given type within the game.", " ", C.green + "Weapons available",
						C.gray + "- " + C.white + "Sword", C.gray + "- " + C.white + "Bow",
						C.gray + "- " + C.white + "Axe", });
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		new GuiBAItemRename(gui.getPlugin(), player, _manager).openInventory();
	}
}
