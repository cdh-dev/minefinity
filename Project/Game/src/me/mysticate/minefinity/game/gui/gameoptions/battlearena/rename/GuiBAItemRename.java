package me.mysticate.minefinity.game.gui.gameoptions.battlearena.rename;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.plugin.java.JavaPlugin;

import me.mysticate.minefinity.core.currency.event.CurrencyChangeEvent;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.core.gui.buttons.BackButton;
import me.mysticate.minefinity.core.gui.buttons.BackButton.ButtonReturn;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.GameType;
import me.mysticate.minefinity.game.gui.gameoptions.ButtonRenameItem;
import me.mysticate.minefinity.game.gui.gameoptions.battlearena.GuiOptionsBA;

public class GuiBAItemRename extends Gui
{
	private GameManager _manager;

	public GuiBAItemRename(JavaPlugin plugin, Player player, GameManager manager)
	{
		super(plugin, player, "Item Renaming", 27);

		_manager = manager;
	}

	@EventHandler
	public void onCurrency(CurrencyChangeEvent event)
	{
		if (event.getPlayer() == getPlayer())
			updateInventory();
	}

	@Override
	protected void addItems()
	{
		setItem(0, new BackButton(new ButtonReturn()
		{
			@Override
			public void onReturn(Gui gui, Player player, ClickType type)
			{
				new GuiOptionsBA(getPlugin(), player, _manager).openInventory();
			}
		}, "Options"));

		borders(true);

		addItem(new ButtonRenameItem(_manager, GameType.BATTLE_ARENA, "name-sword", "Sword", Material.DIAMOND_SWORD,
				(byte) 0));
		addItem(new ButtonRenameItem(_manager, GameType.BATTLE_ARENA, "name-bow", "Bow", Material.BOW, (byte) 0));
		addItem(new ButtonRenameItem(_manager, GameType.BATTLE_ARENA, "name-axe", "Axe", Material.IRON_AXE, (byte) 0));
	}

	@Override
	protected void onOpen()
	{

	}

	@Override
	protected void onClose()
	{

	}
}
