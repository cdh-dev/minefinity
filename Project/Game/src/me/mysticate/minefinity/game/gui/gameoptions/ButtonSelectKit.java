package me.mysticate.minefinity.game.gui.gameoptions;

import java.util.LinkedList;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import me.mysticate.minefinity.core.common.text.C;
import me.mysticate.minefinity.core.common.util.UtilItemStack;
import me.mysticate.minefinity.core.gui.Button;
import me.mysticate.minefinity.core.gui.Gui;
import me.mysticate.minefinity.game.GameManager;
import me.mysticate.minefinity.game.GameType;
import me.mysticate.minefinity.game.kit.KitPlayer;
import me.mysticate.minefinity.game.kit.kit.IKit;
import me.mysticate.minefinity.game.kit.kit.Kit;

public class ButtonSelectKit implements Button
{
	private GameManager _manager;
	private GameType _game;
	private IKit<?> _kit;

	public ButtonSelectKit(GameManager manager, GameType game, IKit<?> kit)
	{
		_manager = manager;
		_game = game;
		_kit = kit;
	}

	private boolean owns(Player player)
	{
		return _kit.isFree()
				|| _manager.getPerkManager().getPlayer(player).hasPerk(Kit.getPerkName(_game, _kit.getId(), Kit.Level));
	}

	private int level(Player player)
	{
		if (!owns(player))
			return 0;

		return Math.max(1,
				_manager.getPerkManager().getPlayer(player).getLevel(Kit.getPerkName(_game, _kit.getId(), Kit.Level)));
	}

	private int magic(Player player)
	{
		return _manager.getPerkManager().getPlayer(player).getLevel(Kit.getPerkName(_game, _kit.getId(), Kit.Magic));
	}

	@Override
	public ItemStack getItem(Gui gui, Player player)
	{
		boolean selected = false;

		KitPlayer kitPlayer = _manager.getKitManager().getPlayer(player);
		int level = level(player);
		int magic = magic(player);

		LinkedList<String> lore = new LinkedList<String>();
		lore.add(level > 0 ? (C.green + "Unlocked") : (C.red + "Locked"));
		lore.add(" ");

		for (String desc : _kit.getDisplayLore())
			lore.add(C.gray + desc);

		lore.add(" ");
		lore.add(C.aqua + "Level " + C.white + ((level > 0) ? level : "-"));

		if (level > 0 && magic > 0)
			lore.add(C.aqua + "Skill " + C.white + magic);

		lore.add(" ");

		if (level > 0)
		{
			if (kitPlayer.hasKit(_game) && kitPlayer.getKit(_game) == _kit.getId())
			{
				selected = true;
				lore.add(C.dGreenBold + "Selected");
			} else
			{
				lore.add(C.greenBold + "Click to Select");
			}
		} else
		{
			lore.add(C.redBold + "Purchase in the Shop");
		}

		ItemStack item = UtilItemStack.create(_kit.getDisplayMat(), _kit.getDisplayData(), 1,
				C.yellowBold + _kit.getDisplayName(), lore.toArray(new String[0]));
		if (selected)
			item.addEnchantment(UtilItemStack.getGlow(), 1);

		return item;
	}

	@Override
	public void onClick(Gui gui, Player player, ClickType type)
	{
		KitPlayer kitPlayer = _manager.getKitManager().getPlayer(player);

		if (kitPlayer.hasKit(_game) && kitPlayer.getKit(_game) == _kit.getId())
			return;

		if (owns(player))
		{
			kitPlayer.setKit(_game, _kit.getId());
			gui.updateInventory();
		}
	}
}
